Start webpack-dev-server in development mode:

```
npm install
npm start
```

Start Styleguidist in development mode:

```
npm guide
```

Design: https://invis.io/DCTYGGZE534
