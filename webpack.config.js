const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const SpriteLoaderPlugin = require("svg-sprite-loader/plugin");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
  .BundleAnalyzerPlugin;

module.exports = {
  entry: "./src/index.jsx",
  //  optimization: {
  //    usedExports: true,
  //    splitChunks: {
  //      chunks: "all",
  //    },
  //  },
  devtool: process.env.NODE_ENV === "development" ? "inline-source-map" : false,
  output: {
    filename: "[name].bundle.js",
    chunkFilename: "[name].bundle.js",
    path: path.resolve(__dirname, "dist"),
    publicPath: "/",
  },

  devServer: {
    historyApiFallback: true,
  },
  resolve: {
    alias: {
      lib: path.resolve(__dirname, "src/lib"),
      containers: path.resolve(__dirname, "src/containers"),
      routes: path.resolve(__dirname, "src/routes"),
      "@": path.resolve(__dirname, "src/"),
    },
    extensions: [".js", ".jsx", ".scss", ".css", ".svg"],
  },
  module: {
    rules: [
      { test: /\.css$/, use: "css-loader" },
      {
        test: /\.module\.s(a|c)ss$/,
        use: [
          "style-loader",
          {
            loader: "css-loader",
            options: {
              modules: {
                localIdentName: "[path]__[local]--[hash:base64:5]",
              },
              sourceMap: true,
            },
          },
          {
            loader: "resolve-url-loader",
            options: {
              debug: true,
            },
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: true,
            },
          },
        ],
      },
      {
        test: /\.s(a|c)ss$/,
        exclude: /\.module.(s(a|c)ss)$/,
        use: [
          "style-loader",
          "css-loader",
          {
            loader: "resolve-url-loader",
            options: {
              debug: true,
            },
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: true,
            },
          },
        ],
      },
      // {
      //   test: /\.css$/,
      //   use: ["style-loader", "css-loader"]
      // },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env", "@babel/preset-react"],
            plugins: [["@babel/plugin-proposal-class-properties"]],
          },
        },
      },

      {
        test: /\.(woff|woff2|eot|ttf|otf|png)$/,
        use: ["file-loader"],
      },

      {
        test: /\.(png|jpg|gif|svg)$/,
        include: [
          path.resolve(__dirname, "src/assets/svg"),
          path.resolve(__dirname, "src/assets/img"),
        ],
        use: [
          {
            loader: "file-loader",
          },
        ],
      },
      {
        test: /\.svg$/,
        exclude: [/node_modules/, path.resolve(__dirname, "src/assets/svg")],
        use: [
          {
            loader: "@svgr/webpack",
          },
        ],
      },
      // {
      //   test: /\.svg$/,
      //   include: path.resolve(__dirname, "src/assets/sprites"),
      //   use: [
      //     {
      //       loader: "svg-sprite-loader"
      //     }
      //   ]
      // }
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/assets/index.html",
    }),
    new SpriteLoaderPlugin({
      spriteAttrs: {
        fill: "currentColor",
      },
    }),
    //    new BundleAnalyzerPlugin(),
  ],
};
