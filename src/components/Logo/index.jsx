import React from "react";
import { Link } from "react-router-dom";

import s from "./style.module.scss";
import SiteLogo from "../../assets/sprites/logo.svg";

export default function Logo() {
  return (
    <Link className={s.logoLink} to="/exchanger">
      <SiteLogo className={s.logoIcon} />
    </Link>
  );
}
