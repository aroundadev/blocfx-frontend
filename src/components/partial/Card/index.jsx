import React, { Component } from 'react';
import s from './style.module.scss';
import '../../../assets/sprites/new-document.svg';


export default class Card extends Component {
    render() {
        const {title, description, children} = this.props;

        return (
            <div className={s.card}>
                <div className={s.card__media}>
                    <svg className={s.card__icon}>{children}</svg>
                </div>
                <p className={s.card__title}>{title}</p>
                <p className={s.card__text}>{description}</p>
            </div>
        );
    }
}
