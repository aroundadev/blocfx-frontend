import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Portal from '../Portal/Portal';
import cx from 'classnames';
import s from './style.module.scss';


class Modal extends Component {
  state = {
    isVisible: false,
  };

  componentDidUpdate(prevProps) {
    if (this.props.isOpen !== prevProps.isOpen && this.props.isOpen) {
      setTimeout(() => {
        this.setState({isVisible: true});
      }, 100);
    }

    if (this.props.isOpen !== prevProps.isOpen && !this.props.isOpen) {
      this.setState({isVisible: false});
    }
  }

  render() {
    const {title, isOpen, onCancel, onSubmit, children,} = this.props;

    return (
        <>
          {isOpen &&
          <Portal>
            <div className={cx(s['modal__overlay'], {[s['modal__overlay--visible']]: this.state.isVisible})}>
              <div className={s['modal__window']}>
                <div className={s['modal__header']}>
                  <p className={s['modal__title']}>{title}</p>
                </div>
                <div className={s['modal__body']}>{children}</div>
                <div className={s['modal__footer']}>
                  {/*<Button onClick={onCancel}>Cancel</Button>*/}
                  <button onClick={onSubmit} className={`btn btn--primary`}>Ok</button>
                </div>
              </div>
            </div>
          </Portal>
          }
        </>
    );
  }
}

Modal.propTypes = {
  title: PropTypes.string,
  isOpen: PropTypes.bool,
  onCancel: PropTypes.func,
  onSubmit: PropTypes.func,
  children: PropTypes.node,
};

Modal.defaultProps = {
  title: 'Modal title',
  isOpen: false,
  onCancel: () => {
  },
  onSubmit: () => {
  },
  children: null,
};

export default Modal;
