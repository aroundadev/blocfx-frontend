import React, { Component } from 'react';
import s from './style.module.scss';


export default class NumerableItem extends Component {
    render() {
        const {number, title, description} = this.props;

        return (
            <div className={s.main}>
                <div className={s.left}>
                    <div className={s.count}>
                        <div className={s.number}>{number}</div>
                    </div>
                </div>

                <div className={s.right}>
                    <p className={s.title}>{title}</p>
                    <p className={s.text}>{description}</p>
                </div>
            </div>
        );
    }
}
