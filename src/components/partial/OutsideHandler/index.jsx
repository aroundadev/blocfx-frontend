import React, { Component } from "react";
import PropTypes from "prop-types";

export default class OutsideHandler extends Component {
  constructor(props) {
    super(props);

    this.clickHandler = props.clickHandler;
    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  componentDidMount() {
    document.addEventListener("mousedown", this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleClickOutside);
  }

  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  handleClickOutside(event) {
    console.log("XXXXXXXXXXXXXX");

    if (
      this.props.canHandleClick &&
      this.wrapperRef &&
      !this.wrapperRef.contains(event.target)
    ) {
      this.clickHandler();
    }
  }

  render() {
    return <div ref={this.setWrapperRef}>{this.props.children}</div>;
  }
}

OutsideHandler.propTypes = {
  children: PropTypes.element.isRequired,
  canHandleClick: PropTypes.bool,
  clickHandler: PropTypes.func
};

OutsideHandler.defaultProps = {
  canHandleClick: false,
  clickHandler: () => {}
};
