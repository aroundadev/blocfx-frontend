import React from "react";
import s from './style.module.scss'

export default ({ children }) => {
  return <div className={s.container}>{children}</div>;
};
