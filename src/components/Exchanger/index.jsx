import React, { useState } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import Header from "@/components/Header";
import Converter from "@/containers/Converter";
// import Converter from "@/components/Converter";
// import FiatToken from "@/components/FiatToken";
// import TransactionDone from "@/components/TransactionDone";

import OtcDesk from "@/components/OtcDesk";
import Settings from "@/components/Settings";
import History from "@/components/History";
import ChangePassword from "@/components/ChangePassword";
import ToggleMenu from "lib/Menu/ToggleMenu";

import FiatTokenPayment from "routes/FiatTokenPayment";
import FiatToken from "routes/FiatToken";
import FiatManualPayment from "routes/FiatManualPayment";
import FiatManual from "routes/FiatManual";
import CryptoManualPayment from "routes/CryptoManualPayment";
import CryptoManual from "routes/CryptoManual";
import Beneficiaries from "routes/Beneficiaries";
import AddBeneficiary from "routes/AddBeneficiary";
import PaymentAccounts from "routes/PaymentAccounts";
import AddPaymentAccount from "routes/AddPaymentAccount";

import s from "./style.module.scss";

export default function Exchanger() {
  const [isOpen, setIsOpen] = useState(false);
  const openMenu = () => setIsOpen(true);
  const closeMenu = () => setIsOpen(false);
  return (
    <div className={s.layout}>
      <header className={s.headerContainer}>
        <Header openMenu={openMenu} />
      </header>
      <main className={s.contentContainer}>
        <Switch>
          <Route path="/exchanger/converter">
            <Converter />
          </Route>
          <Route path="/exchanger/beneficiaries/add">
            <AddBeneficiary />
          </Route>
          <Route path="/exchanger/beneficiaries">
            <Beneficiaries />
          </Route>
          <Route path="/exchanger/payment-accounts/add">
            <AddPaymentAccount />
          </Route>
          <Route path="/exchanger/payment-accounts">
            <PaymentAccounts />
          </Route>

          <Route path="/exchanger/crypto-manual/payment">
            <CryptoManualPayment />
          </Route>
          <Route path="/exchanger/crypto-manual">
            <CryptoManual />
          </Route>
          <Route path="/exchanger/fiat-manual/payment">
            <FiatManualPayment />
          </Route>
          <Route path="/exchanger/fiat-manual">
            <FiatManual />
          </Route>
          <Route path="/exchanger/fiat-token/payment">
            <FiatTokenPayment />
          </Route>
          <Route path="/exchanger/fiat-token">
            <FiatToken />
          </Route>
          <Route path="/exchanger/otc-desk">
            <OtcDesk />
          </Route>
          <Route path="/exchanger/settings">
            <Settings />
          </Route>
          <Route path="/exchanger/history">
            <History />
          </Route>
          <Route path="/exchanger/change-password">
            <ChangePassword />
          </Route>
          <Redirect to="/exchanger/converter" />
        </Switch>
      </main>
      {isOpen && <ToggleMenu closeMenu={closeMenu} />}
    </div>
  );
}
