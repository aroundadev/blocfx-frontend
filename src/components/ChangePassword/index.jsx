import React, {Component} from "react";
import s from "./style.module.scss";
import cx from "classnames";

export default class ChangePassword extends Component {
    state = {
        currentPswd: '000000000',
        newPswd: '',
        confirmPswd: ''
    };

    setValue = (event, field) => {
        event.preventDefault();

        this.setState({
            [field]: event.target.value
        });
    };

    handleSubmit = (event) => {
        event.preventDefault();
    };

    render() {
        const {currentPswd, newPswd, confirmPswd} = this.state;

        return (
            <div>
                <h1 className="title">Change Password</h1>

                <form className={s.form} onSubmit={this.handleSubmit}>
                    <div className={s.fields}>
                        <div className={s.field}>
                            <label htmlFor="email" className={s.fieldLabel}>Current password</label>
                            <input
                                id="currentPassword"
                                type="password"
                                value={currentPswd}
                                className={s.fieldInput}
                                onChange={ev => this.setValue(ev, 'currentPswd')}
                            />
                        </div>
                        <div className={s.field}>
                            <label htmlFor="email" className={s.fieldLabel}>Current password</label>
                            <input
                                id="newPassword"
                                type="password"
                                value={newPswd}
                                className={s.fieldInput}
                                onChange={ev => this.setValue(ev, 'newPswd')}
                            />
                        </div>
                        <div className={s.field}>
                            <label htmlFor="email" className={s.fieldLabel}>Current password</label>
                            <input
                                id="confirmPassword"
                                type="password"
                                value={confirmPswd}
                                className={s.fieldInput}
                                onChange={ev => this.setValue(ev, 'confirmPswd')}
                            />
                        </div>
                    </div>
                    <div className={s.controls}>
                        <button className={`btn btn--primary`} onClick={this.handleSubmit}>
                            Save changes
                        </button>
                    </div>
                </form>
            </div>
        );
    }
}