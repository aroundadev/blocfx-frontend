import React, { useState, Suspense } from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect,
} from "react-router-dom";
//import Exchanger from "@/components/Exchanger";
import Login from "routes/Login";
import Password from "routes/Password";
const Exchanger = React.lazy(() => import("../components/Exchanger"));

///pass for dev version

const pass = "Jigzo12345";

export default function App() {
  /// login for dev version, remove on prod

  const [isLogin, setIsLogin] = useState(false);
  const [value, setValue] = useState("");

  const handleInput = (e) => {
    setValue(e.target.value);
  };

  const handleSubmit = () => {
    if (value === pass) {
      setIsLogin(true);
    }
  };

  ///end of login

  return (
    <Router>
      {!isLogin ? (
        <Password handleInput={handleInput} handleSubmit={handleSubmit} />
      ) : (
        <Switch>
          <Route path="/login" component={Login} />
          <Route path="/exchanger">
            <Suspense fallback={<div>Loading...</div>}>
              <Exchanger />
            </Suspense>
          </Route>
          <Route path="/registration" component={Registration} />
          <Redirect to="/exchanger" />
        </Switch>
      )}
    </Router>
  );
}

function Registration() {
  return <h1>REGISTRATION</h1>;
}
