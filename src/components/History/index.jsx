import React, {Component} from "react";

import cx from 'classnames';
import s from "./style.module.scss";


import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';

import { DateRangePicker } from 'react-dates';


class History extends Component {
  constructor(props) { 
    super(props);
    this.state = {
      startDate: null,
      endDate: null,
      focusedInput: null,
    };

  }


  render() {

  const startDate = new Date();


    return (

      <>
        <h1 className="title">History</h1>



        <div className={s.history__nav}>
          <ul className={cx(s.history__menu, s.menu)}>
            <li className={cx(s.menu__item, s.current)}><a href="#">All</a></li>
            <li className={s.menu__item}><a href="#">Done</a></li>
            <li className={s.menu__item}><a href="#">Processing</a></li>
            <li className={s.menu__item}><a href="#">Canceled</a></li>
          </ul>
          <div className={s.history__datepicker_wrap}>  
            <div className={s.history__datepicker_toggle}>
              <span className={s.calendar__icon}></span>
            </div>
            <div className={s.history__datepicker}> 
              <span className={s.calendar__icon}></span>
              
              <DateRangePicker
                endDateId="endDate"
                startDate={this.state.startDate}
                endDate={this.state.endDate}
                small= {true}
                hideKeyboardShortcutsPanel
                weekDayFormat="dd"
                noBorder
                firstDayOfWeek={1}
                anchorDirection="right"
                daySize={41}
                startDateId="startDate"
                startDatePlaceholderText=""
                endDatePlaceholderText=""
                displayFormat="DD.MM.YY"
                onDatesChange={({ startDate, endDate }) => { this.setState({ startDate, endDate })}}
                focusedInput={this.state.focusedInput}
                onFocusChange={(focusedInput) => { this.setState({ focusedInput })}}
              />
            </div>
          </div>
        </div>

        <div className={s.history__content}>
          <div className={s.table}>
            <div className={s.table__head}>
              <div className={s.table__row}>
                <div className={s.table__cell}>Transaction</div>
                <div className={s.table__cell}>Status</div>
                <div className={s.table__cell}>Date</div>
                <div className={s.table__cell}>Sent</div>
                <div className={s.table__cell}>Recieved</div>
              </div>
            </div>
            <div className={s.table__body}>
              <div className={s.table__row}>
                <div className={s.table__cell} data-title="Transaction">ETH to BTC</div>
                <div className={s.table__cell} data-title="Status">
                  <div className={s.status}><span className={cx(s.dot, s.done)}></span>Done</div>
                </div>
                <div className={s.table__cell} data-title="Date">Nov 4, 2019 at 16:14</div>
                <div className={s.table__cell} data-title="Sent">48,17 ETH</div>
                <div className={s.table__cell} data-title="Recieved">1 BTC</div>
              </div>

              <div className={s.table__row}>
                <div className={s.table__cell} data-title="Transaction">USD to BTC</div>
                <div className={s.table__cell} data-title="Status">
                  <div className={s.status}><span className={cx(s.dot, s.processing)}></span>Processing</div>
                </div>
                <div className={s.table__cell} data-title="Date">Nov 4, 2019 at 12:10</div>
                <div className={s.table__cell} data-title="Sent">9851,53 USD</div>
                <div className={s.table__cell} data-title="Recieved">1 BTC</div>
              </div>

              <div className={s.table__row}>
                <div className={s.table__cell} data-title="Transaction">USD to EUR</div>
                <div className={s.table__cell} data-title="Status">
                  <div className={s.status}><span className={cx(s.dot, s.canceled)}></span>Canceled</div>
                </div>
                <div className={s.table__cell} data-title="Date">Nov 3, 2019 at 21:43</div>
                <div className={s.table__cell} data-title="Sent">200 USD</div>
                <div className={s.table__cell} data-title="Recieved">181.05 EUR</div>
              </div>
              <div className={s.table__row}>
                <div className={s.table__cell} data-title="Transaction">ETH to BTC</div>
                <div className={s.table__cell} data-title="Status">
                  <div className={s.status}><span className={cx(s.dot, s.done)}></span>Done</div>
                </div>
                <div className={s.table__cell} data-title="Date">Nov 4, 2019 at 16:14</div>
                <div className={s.table__cell} data-title="Sent">48,17 ETH</div>
                <div className={s.table__cell} data-title="Recieved">1 BTC</div>
              </div>

              <div className={s.table__row}>
                <div className={s.table__cell} data-title="Transaction">USD to BTC</div>
                <div className={s.table__cell} data-title="Status">
                  <div className={s.status}><span className={cx(s.dot, s.processing)}></span>Processing</div>
                </div>
                <div className={s.table__cell} data-title="Date">Nov 4, 2019 at 12:10</div>
                <div className={s.table__cell} data-title="Sent">9851,53 USD</div>
                <div className={s.table__cell} data-title="Recieved">1 BTC</div>
              </div>

              <div className={s.table__row}>
                <div className={s.table__cell} data-title="Transaction">USD to EUR</div> 
                <div className={s.table__cell} data-title="Status">
                  <div className={s.status}><span className={cx(s.dot, s.canceled)}></span>Canceled</div>
                </div>
                <div className={s.table__cell} data-title="Date">Nov 3, 2019 at 21:43</div>
                <div className={s.table__cell} data-title="Sent">200 USD</div>
                <div className={s.table__cell} data-title="Recieved">181.05 EUR</div>
              </div>
              <div className={s.table__row}>
                <div className={s.table__cell} data-title="Transaction">ETH to BTC</div>
                <div className={s.table__cell} data-title="Status">
                  <div className={s.status}><span className={cx(s.dot, s.done)}></span>Done</div>
                </div>
                <div className={s.table__cell} data-title="Date">Nov 4, 2019 at 16:14</div>
                <div className={s.table__cell} data-title="Sent">48,17 ETH</div>
                <div className={s.table__cell} data-title="Recieved">1 BTC</div>
              </div>

              <div className={s.table__row}>
                <div className={s.table__cell} data-title="Transaction">USD to BTC</div>
                <div className={s.table__cell} data-title="Status">
                  <div className={s.status}><span className={cx(s.dot, s.processing)}></span>Processing</div>
                </div> 
                <div className={s.table__cell} data-title="Date">Nov 4, 2019 at 12:10</div>
                <div className={s.table__cell} data-title="Sent">9851,53 USD</div>
                <div className={s.table__cell} data-title="Recieved">1 BTC</div>
              </div>

              <div className={s.table__row}>
                <div className={s.table__cell} data-title="Transaction">USD to EUR</div>
                <div className={s.table__cell} data-title="Status">
                  <div className={s.status}><span className={cx(s.dot, s.canceled)}></span>Canceled</div>
                </div>
                <div className={s.table__cell} data-title="Date">Nov 3, 2019 at 21:43</div>
                <div className={s.table__cell} data-title="Sent">200 USD</div>
                <div className={s.table__cell} data-title="Recieved">181.05 EUR</div>
              </div>
            </div>
          </div>
        </div>

      </>
    );
  }
}

export default History;
