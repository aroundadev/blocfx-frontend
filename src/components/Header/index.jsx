import React from "react";
import { Link } from "react-router-dom";
import MainMenu from "lib/Menu/MainMenu";
import s from "./style.module.scss";
import BurgButton from "../../assets/sprites/burger-btn.svg";
import SiteLogo from "lib/SiteLogo";

export default function Header({ openMenu }) {
  return (
    <div className={s.header}>
      <Link to="/exchager">
        <SiteLogo className={s.siteLogo} color="dark" />
      </Link>
      <BurgButton onClick={openMenu} className={s.burgerBtn} />
      <MainMenu className={s.mainMenu} />
    </div>
  );
}
