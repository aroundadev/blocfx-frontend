import React from "react";
import Card from "@/components/partial/Card";
import NumerableItem from "@/components/partial/NumerableItem";
import cx from "classnames";
import s from "./style.module.scss";

import "@/assets/sprites/wallet.svg";
import "@/assets/sprites/padlock.svg";
import "@/assets/sprites/new-document.svg";

import WalletIcon from "@/assets/sprites/wallet.svg";
import PadlockIcon from "@/assets/sprites/padlock.svg";
import NewDocumentIcon from "@/assets/sprites/new-document.svg";

export default function OtcDesk() {
  const cards = [
    {
      title: "Order size",
      icon: WalletIcon,
      description:
        "The minimum order size is $100,000 but exceptions can be discussed."
    },
    {
      title: "Assets",
      icon: PadlockIcon,
      description:
        "BlocFX OTC does not custody assets on behalf of trading counterparties."
    },
    {
      title: "Eligibility",
      icon: NewDocumentIcon,
      description:
        "Eligibility for OTC trading are subject to AML/KYC and other requirements."
    }
  ];

  const items = [
    {
      title: "Sign up & Onboarding",
      description:
        "When you decide you want to buy or sell a large amount of digital assets over-the-counter, registr and verify yourself with documents. To become an OTC client you must go through the standard BlocFX onboarding process, obtaining the highest level of verification, to enable large transactions."
    },
    {
      title: "Time and methods",
      description:
        "Select suitable time slot on Calendly. Then select preferred method:" +
        "- Phone" +
        "- In person at BlocFX office"
    },
    {
      title: "Rate agreement",
      description:
        "Admin provides exchange rate to the customer. Once rate is agreed, admin creates DocuSign with the trade details." +
        "When DocuSign agreement is signed the trade is created on SalesForce."
    },
    {
      title: "Complete trade",
      description:
        "Admins manually process the orders and once trade is completed customer receives confirmation on specified email."
    }
  ];

  return (
    <div>
      <h1 className="title">OTC Desk</h1>

      <section className={cx(s.section, s["section--description"])}>
        <div className={s.section__inner}>
          <p>
            Over-the-counter (OTC) trading takes place off the open BlocFX
            exchange. We offer deeper liquidity and a private, more personalized
            service to institutions and high net-worth individuals needing to
            fill large orders that might be too disruptive if placed on open
            markets at the exchanges.
          </p>
          <p>
            Whether you are trading blocks of $100,000, €10,000,000 or 2,000
            Bitcoin, the OTC desk will provide you with execution and settlement
            services that are discrete, secure and ultra-competitive. With OTC
            user has the option to pay by bank transfer, blockchain or cash.
            User can complete the trade over the phone or in person.
          </p>
        </div>
      </section>

      <section className={cx(s.section, s["section--cards"])}>
        <h2 className="section__title">Terms and Eligibility</h2>
        <div className={s.section__inner}>
          {cards.map((data, index) => {
            return (
              <div className={s["card-item"]} key={index}>
                <Card {...data}>{data.icon()}</Card>
              </div>
            );
          })}
        </div>
      </section>

      <section className={cx(s.section, s["section--list"])}>
        <h2 className="section__title">How it works</h2>
        <div className={s.section__inner}>
          {items.map((data, index) => {
            return (
              <div className={s["list-item"]} key={index}>
                <NumerableItem {...data} number={++index} />
              </div>
            );
          })}
        </div>
      </section>
    </div>
  );
}
