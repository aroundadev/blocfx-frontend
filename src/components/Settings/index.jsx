import React, {Component} from "react";
import PropTypes from 'prop-types';
import Modal from "../partial/Modal";
import OutsideHandler from "../partial/OutsideHandler";
import cx from 'classnames';
import s from "./style.module.scss";

const codes = [
  {
    code: '1',
    abbreviation: 'USA',
    country: 'United States of America'
  },
  {
    code: '44',
    abbreviation: 'UK',
    country: 'United Kingdom'
  },
  {
    code: '256',
    abbreviation: 'UG',
    country: 'Uganda'
  },
  {
    code: '380',
    abbreviation: 'UA',
    country: 'Ukraine'
  },
];


class Settings extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: props.email,
      phoneCode: props.phoneCode,
      phoneNumber: props.phoneNumber,
      filterValue: '',
      isOpen: false,
      showSelect: false
    };
  }

  openModal = () => {
    this.setState({isOpen: true});
  };

  closeModal = () => {
    this.setState({isOpen: false});
  };

  handleSubmit = (event) => {
    event.preventDefault();
  };

  setValue = (event, field) => {
    event.preventDefault();

    this.setState({
      [field]: event.target.value
    });
  };

  setNumber = (event, field) => {
    event.preventDefault();
    this.setState({[field]: event.target.value.replace(/\D+/g, '')});
  };

  togglePhoneSelect = () => {
    this.setState(prevState => ({
      showSelect: !prevState.showSelect
    }));
  };

  getPhoneCode = () => {
    const result = codes.find(item => item.code === this.state.phoneCode);
    return `${result.abbreviation} (+${result.code})`;
  };

  selectNewPhoneCode = (phoneCode) => {
    this.setState({phoneCode, showSelect: false});
  };

  filter = (event) => {
    event.preventDefault();

    this.setState({
      filterValue: event.target.value
    })
  };

  renderSelectPhoneCode = () => {
    const {filterValue} = this.state;

    return (
      <>
        <OutsideHandler canHandleClick={this.state.showSelect} clickHandler={this.togglePhoneSelect}>
          <div className={cx(
            s.selectPhoneCode,
            {[s.hide]: !this.state.showSelect}
          )}>
            <input className={s.search} value={filterValue} onChange={ev => this.filter(ev)}
                 placeholder="Search"/>
            {codes.map((currElem, index) => {
              const comparable = '+' + Object.values(currElem).join('').split(' ').join('').toLowerCase();
              const filter = filterValue.split(' ').join().toLowerCase();

              if (comparable.indexOf(filter) !== -1) {
                return (
                  <div key={index}
                     className={cx(s.selectItem, {[s.selectItemActive]: currElem.code === this.state.phoneCode})}
                     onClick={() => this.selectNewPhoneCode(currElem.code)}>
                    <span>{currElem.abbreviation} (+{currElem.code})</span>
                    <div>{currElem.country}</div>
                  </div>
                )
              }
            })}
          </div>
        </OutsideHandler>
      </>
    )
  };

  render() {
    const {email, phoneNumber} = this.state;

    return (
      <>
        <h1 className="title">Settings</h1>

        <form className={s.form} onSubmit={this.handleSubmit}>
          <div className={s.fields}>
            <div className={cx(s.field, s.email)}>
              <label htmlFor="email" className={s.fieldLabel}>Email</label>
              <input
                id="email"
                type="email"
                value={email}
                className={s.fieldInput}
                onChange={ev => this.setValue(ev, 'email')}
              />
            </div>
            <div className={s.field}>
              <label htmlFor="phone" className={s.fieldLabel}>Mobile Phone</label>
              <div className={s.fieldContainer}>
                <div className={s.phoneCode}>
                  <div className={s.fieldInput} onClick={() => this.togglePhoneSelect()}>
                    {this.getPhoneCode()}
                  </div>
                  {this.renderSelectPhoneCode()}
                </div>
                <input
                  id="phone"
                  type="text"
                  pattern="[0-9]*"
                  value={phoneNumber}
                  className={s.fieldInput}
                  onChange={ev => this.setNumber(ev, 'phoneNumber')}
                />
              </div>
            </div>
          </div>
          <div className={s.controls}>
            <button className={`btn btn--default`} onClick={this.openModal}>Change Password</button>
            <button
              className={`btn btn--primary`}
              onClick={this.handleSubmit}
            >
              Save changes
            </button>
          </div>
        </form>

        <Modal
          title="Password change"
          isOpen={this.state.isOpen}
          onSubmit={this.closeModal}
        >
          <p>We sent you a link via email.</p>
        </Modal>
      </>
    );
  }
}

Settings.propTypes = {
  email: PropTypes.string,
  phoneCode: PropTypes.string,
  phoneNumber: PropTypes.string,
};

Settings.defaultProps = {
  email: "test_test@test.test",
  phoneCode: '1',
  phoneNumber: '6499150024'
};

export default Settings;
