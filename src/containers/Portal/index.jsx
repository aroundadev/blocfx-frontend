import React, { useEffect } from "react";
import ReactDOM from "react-dom";

export default function Portal({ children }) {
  const el = document.createElement("div");

  useEffect(() => {
    const modalRoot = document.getElementById("modal-root");
    modalRoot.appendChild(el);
  });

  return ReactDOM.createPortal(children, el);
}
