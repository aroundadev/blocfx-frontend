import React, { useState } from "react";
import Title from "lib/Title";
import Modal from "lib/Modal";
import Tabs, { Tab, TabTitle, TabContent } from "lib/Tabs";
import BlockchainForm from "containers/BlockchainForm";
import BankAccForm from "containers/BankAccForm";

import s from "./style.module.scss";

export default function AddAccountModal({ startTab, closeModal }) {
  return (
    <Modal>
      <div className={s.wrapper}>
        <Title className={s.title} size={Title.H1}>
          Add Beneficiary
        </Title>
        <Tabs startTab={startTab}>
          <Tab>
            <TabTitle>Blockchain</TabTitle>
            <TabContent>
              <BlockchainForm closeModal={closeModal} />
            </TabContent>
          </Tab>
          <Tab>
            <TabTitle>Bank account</TabTitle>
            <TabContent>
              <BankAccForm closeModal={closeModal} />
            </TabContent>
          </Tab>
        </Tabs>
      </div>
    </Modal>
  );
}
