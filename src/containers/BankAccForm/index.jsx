import React, { useState } from "react";

import Input from "lib/Input";
import SelectCurrency from "lib/Select/SelectCurrency";
import ModalButtons from "lib/ModalButtons";
import SelectCountry from "lib/Select/SelectCountry";

import s from "./style.module.scss";

const fsInit = {};

export default function BankAccForm({ primButtonAction, secondButtonAction }) {
  const [fs, setFs] = useState(fsInit);

  const handleChange = e => {
    const { name, value } = e.target;
    setFs({
      ...fs,
      [name]: value
    });
  };

  return (
    <form>
      <div className={s.wrapper}>
        <Input
          className={s.name}
          label="Name of beneficiary"
          placeholder="Enter name of beneficiary"
          name="name"
          value={fs.name}
          onChange={handleChange}
        />
        <Input
          label="Bank name"
          placeholder="Enter bank name"
          className={s.bankName}
          name="bankName"
          value={fs.bankName}
          onChange={handleChange}
        />
        <Input
          className={s.iban}
          label="IBAN"
          placeholder="Enter IBAN"
          name="iban"
          value={fs.iban}
          onChange={handleChange}
        />
        <SelectCurrency
          className={s.currency}
          name="currency"
          label="Currency"
          placeholder="Select the default currency"
          onChange={handleChange}
          showLabel={true}
          dynamicLabel={false}
          value={fs.currency}
        />
        <Input
          className={s.address}
          label="Address of beneficiary"
          placeholder="Enter address of beneficiary"
          name="address"
          value={fs.address}
          onChange={handleChange}
        />
        <Input
          className={s.bankAddress}
          label="Bank Address"
          placeholder="Enter bank address"
          name="bankAddress"
          value={fs.bankAddress}
          onChange={handleChange}
        />
        <SelectCountry className={s.country} name="country" />
        <Input
          className={s.bankCode}
          label="Bank SWIFT/BIC code"
          placeholder="Enter beneficiary bank SWIFT/BIC code"
          name="bankCode"
          value={fs.bahkCode}
          onChange={handleChange}
        />
        <Input
          className={s.reference}
          label="Reference"
          placeholder="Enter reference"
          name="reference"
          value={fs.reference}
          onChange={handleChange}
        />
      </div>
      <ModalButtons
        primaryAction={primButtonAction}
        secondaryAction={primButtonAction}
      />
    </form>
  );
}
