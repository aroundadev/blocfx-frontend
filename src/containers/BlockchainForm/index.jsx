import React, { useState } from "react";

import Input from "lib/Input";
import SelectCurrency from "lib/Select/SelectCurrency";
import ModalButtons from "lib/ModalButtons";

import s from "./style.module.scss";

import currencies from "../../data/fake/currencies";

const fsInit = {
  address: "",
  currency: currencies.find(c => c.name === "Etherium"),
  bankName: ""
};

export default function BlockchainForm({
  primButtonAction,
  secondButtonAction
}) {
  const [fs, setFs] = useState(fsInit);

  const handleChange = e => {
    const { name, value } = e.target;
    setFs({
      ...fs,
      [name]: value
    });
  };

  return (
    <form>
      <div className={s.wrapper}>
        <Input
          label="Address"
          placeholder="Enter beneficiary address"
          name="address"
          value={fs.address}
          onChange={handleChange}
        />
        <SelectCurrency
          name="currency"
          onChange={handleChange}
          showLabel={false}
          value={fs.currency}
        />
        <Input
          label="Bank name"
          placeholder="Enter bank name"
          className={s.bankName}
          name="bankName"
          value={fs.bankName}
          onChange={handleChange}
        />
      </div>
      <ModalButtons
        primaryAction={primButtonAction}
        secondaryAction={primButtonAction}
      />
    </form>
  );
}
