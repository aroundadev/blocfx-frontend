import React from "react";
import cx from "classnames";
import s from "./style.module.scss";

export default function Panel({ children, className, ...props }) {
  return (
    <div className={cx(s.panel, className)} {...props}>
      {children}
    </div>
  );
}
