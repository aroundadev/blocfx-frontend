import React from "react";
import cx from "classnames";
import s from "./style.module.scss";

export default function PopularCurrencies({ className }) {
  return (
    <div className={cx(s.container, className)}>
      <div>
        <div className={s.code}>BTC</div>
        <div className={s.value}>$ 7000.02</div>
        <div className={cx(s.percent)}>1.63%</div>
      </div>
      <div>
        <div className={s.code}>BTC</div>
        <div className={s.value}>$ 7000.02</div>
        <div className={cx(s.percent, s.down)}>1.63%</div>
      </div>
      <div>
        <div className={s.code}>BTC</div>
        <div className={s.value}>$ 7000.02</div>
        <div className={cx(s.percent)}>1.63%</div>
      </div>
      <div>
        <div className={s.code}>BTC</div>
        <div className={s.value}>$ 7000.02</div>
        <div className={cx(s.percent)}>1.63%</div>
      </div>
      <div>
        <div className={s.code}>BTC</div>
        <div className={s.value}>$ 7000.02</div>
        <div className={cx(s.percent)}>1.63%</div>
      </div>
      <div>
        <div className={s.code}>BTC</div>
        <div className={s.value}>$ 7000.02</div>
        <div className={cx(s.percent, s.up)}>1.63%</div>
      </div>
    </div>
  );
}
