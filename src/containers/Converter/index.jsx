import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import s from "./style.module.scss";
import Title from "lib/Title";
import Card from "lib/Card/Card";
import Button from "lib/Button";
import Panel from "containers/Panel";
import PopularCurrencies from "containers/PopularCurrencies";
import Swap from "lib/Swap";
import Input from "lib/Input";
import SelectCurrency from "lib/Select/SelectCurrency";
import SelectAccount from "lib/Select/SelectAccount";
import ResetButton from "lib/ResetButton";
import Modal from "containers/AddAccountModal";
import AddAccountModal from "../AddAccountModal";

const accs = [
  {
    name: "Reiffeisen Bank",
    code: "USD"
  },
  {
    name: "Unicredit Bank",
    code: "USD"
  }
];
const benefic = [
  {
    name: "John Doe",
    code: "ETH"
  },
  {
    name: "Jane Sanders",
    code: "BTC"
  }
];

const currency = [
  {
    name: "US Dollar",
    code: "USD",
    sign: "$",
    type: "fiat"
  },
  {
    name: "Pound Sterling",
    code: "GBP",
    sign: "£",
    type: "fiat"
  },
  {
    name: "Euro",
    code: "EUR",
    sign: "€",
    type: "fiat"
  },
  {
    name: "Bitcoin",
    code: "BTC",
    icon: "bitcoin",
    type: "crypto"
  },
  {
    name: "Etherium",
    code: "ETH",
    icon: "etherium",
    type: "crypto"
  }
];

const src = {
  value: 100,
  currency: currency[4]
};
const dest = {
  value: 100,
  currency: currency[2]
};

const FIATFIAT = "fiatfiat";
const FIATCRYPTO = "fiatcrypto";
const CRYPTOFIAT = "cryptofiat";
const CRYPTOCRYPTO = "cryptocrypto";

const initState = { src, dest };

const Converter = () => {
  const [converter, setConverterState] = useState(initState);
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [modalTabIndex, setModalTabIndex] = useState(0);
  const history = useHistory();

  const openModal = modalTabIndex => {
    setModalTabIndex(modalTabIndex);
    setModalIsOpen(true);
  };
  const closeModal = () => {
    console.log("CLOSE MODAL");

    setModalIsOpen(false);
  };

  const handleOnChange = e => {
    const { name, value } = e.target;

    const [keyLev1, keyLev2] = name.split(".");

    if (keyLev2) {
      setConverterState({
        ...converter,
        [keyLev1]: {
          ...converter.src,
          [keyLev2]: value
        }
      });
    } else {
      setConverterState({
        ...converter,
        [keyLev1]: value
      });
    }
  };

  const handleSwap = () => {
    const tmp = converter.src;
    setConverterState({
      ...converter,
      src: converter.dest,
      dest: tmp
    });
  };

  const navigate = dir => {
    history.push(`/exchanger/${dir}`);
  };

  function getDirection() {
    return converter.src.currency.type + converter.dest.currency.type;
  }

  const handleClick = () => {
    switch (getDirection()) {
      case FIATCRYPTO:
      case CRYPTOFIAT:
        navigate("fiat-manual");
        break;
      case CRYPTOCRYPTO:
        navigate("crypto-manual");
        break;
      case FIATFIAT:
        navigate("fiat-manual");
        break;
    }
  };

  return (
    <>
      {modalIsOpen && (
        <AddAccountModal startTab={modalTabIndex} closeModal={closeModal} />
      )}
      <Title className={s.mainTitle} size={"h1"}>
        Converter
      </Title>
      <Card className={s.calcSection}>
        <Panel>
          <Input
            name="src.value"
            value={converter.src.value}
            onChange={handleOnChange}
            className={s.value}
            label={"You send"}
            placeholder={"Enter value"}
          />
          <SelectCurrency
            name="src.currency"
            value={converter.src.currency}
            options={currency}
            className={s.currency}
            onChange={handleOnChange}
          />
          <SelectAccount
            style={{
              visibility:
                (getDirection() === CRYPTOCRYPTO && "hidden") || "visible"
            }}
            className={s.account}
            options={accs}
            menuButton="+ Add payment account"
            menuButtonAction={openModal.bind(null, 1)}
            placeholder="Select payment account"
          />
          <p className={s.info}>Min. ammount 198.39 USD</p>
        </Panel>
        <Swap className={s.swap} onClick={handleSwap} />
        <Panel>
          <Input
            name="dest.value"
            value={converter.dest.value}
            onChange={handleOnChange}
            className={s.value}
            label={"You get"}
            placeholder={"Enter value"}
          />
          <SelectCurrency
            name="dest.currency"
            value={converter.dest.currency}
            options={currency}
            className={s.currency}
            onChange={handleOnChange}
          />
          <SelectAccount
            className={s.account}
            options={accs}
            menuButton="+ Add payment account"
            menuButtonAction={openModal.bind(null, 0)}
            placeholder="Select beneficiary"
          />
          <p className={s.info}>1 ETH = 198.39 USD</p>
        </Panel>
      </Card>
      <div className={s.acceptSection}>
        <ResetButton className={s.resetButton} />
        <Button onClick={handleClick} className={s.acceptButton} type="large">
          Accept Rates
        </Button>
      </div>
      <Title className={s.sectionTitle} size={"h2"}>
        Popular currencies
      </Title>
      <Card className={s.popularSection}>
        <PopularCurrencies />
      </Card>
    </>
  );
};

export default Converter;
