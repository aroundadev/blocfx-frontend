import React, { useState } from "react";

import { useHistory } from "react-router-dom";

import Input from "lib/Input";
import SelectCurrency from "lib/Select/SelectCurrency";
import ModalButtons from "lib/ModalButtons";
import SelectCountry from "lib/Select/SelectCountry";

import s from "./style.module.scss";

const fsInit = {};

export default function AddPaymentAccountForm() {
  const [fs, setFs] = useState(fsInit);
  const history = useHistory();

  const handleChange = e => {
    const { name, value } = e.target;
    setFs({
      ...fs,
      [name]: value
    });
  };

  const navigate = () => history.push("/exchanger/payment-accounts");

  return (
    <form>
      <div className={s.wrapper}>
        <Input
          label="Bank name"
          placeholder="Enter bank name"
          className={s.bankName}
          name="bankName"
          value={fs.bankName}
          onChange={handleChange}
        />
        <Input
          className={s.iban}
          label="IBAN"
          placeholder="Enter IBAN"
          name="iban"
          value={fs.iban}
          onChange={handleChange}
        />
        <SelectCurrency
          className={s.currency}
          name="currency"
          label="Currency"
          placeholder="Select the default currency"
          onChange={handleChange}
          showLabel={true}
          dynamicLabel={false}
          value={fs.currency}
        />
        <Input
          className={s.bankAddress}
          label="Bank Address"
          placeholder="Enter bank address"
          name="bankAddress"
          value={fs.bankAddress}
          onChange={handleChange}
        />
        <SelectCountry className={s.country} name="country" />
        <Input
          className={s.bankCode}
          label="Bank SWIFT/BIC code"
          placeholder="Enter beneficiary bank SWIFT/BIC code"
          name="bankCode"
          value={fs.bahkCode}
          onChange={handleChange}
        />
      </div>
      <ModalButtons primaryAction={navigate} secondaryAction={navigate} />
    </form>
  );
}
