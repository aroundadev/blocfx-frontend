InputDisabled component example:

```jsx
const dollar = {
  name: "US Dollar",
  code: "USD",
  sign: "$"
};

const bitcoin = {
  name: "Bitcoin",
  code: "BTC",
  icon: "bitcoin"
};

<div
  style={{
    display: "grid",
    gridTemplateColumns: "1fr  1fr",
    gridColumnGap: "10px"
  }}
>
  <InputDisabled label="With value" value={"$ 1000"} />
  <InputDisabled label="With object" value={dollar} />
  <InputDisabled label="With object" value={bitcoin} />
</div>;
```
