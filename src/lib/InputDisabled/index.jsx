import React from "react";
import cx from "classnames";

import BitcoinIcon from "./svg/bitcoin.svg";
import EtheriumIcon from "./svg/etherium.svg";

const defaultIcons = {
  bitcoin: BitcoinIcon,
  etherium: EtheriumIcon
};

import s from "./style.module.scss";

const isObject = v => typeof v === "object";

export default function InputDisabled({ className, style, label, value }) {
  console.log(value);

  return (
    <div style={style} className={cx(s.container, className)}>
      <div
        className={cx(
          s.label,
          (isObject(value) && s.labelCurrency) || s.labelValue
        )}
      >
        {(isObject(value) && value.name) || label}
      </div>
      <div className={s.input}>
        {(isObject(value) && <CurrencyValue value={value} />) || value}
      </div>
    </div>
  );
}

function CurrencyValue({ value }) {
  const { name, icon, code, sign } = value;
  return (
    <>
      {(sign && <span className={s.sign}>{sign}</span>) ||
        (icon && defaultIcons[icon]({ className: s.icon }))}
      <span className={s.code}>{code}</span>
    </>
  );
}
