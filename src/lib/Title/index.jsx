import React from "react";
import PropTypes from "prop-types";

import cx from "classnames";
import s from "./style.module.scss";

const titleSizes = {
  H1: "h1",
  H2: "h2",
  H3: "h3"
};

/**
 * Main title (H1) of the application
 */

export default function Title({ children, className, size }) {
  switch (size.toLowerCase()) {
    case titleSizes.H1:
      return <h1 className={cx(s.title, s.h1, className)}>{children}</h1>;
    case titleSizes.H2:
      return <h2 className={cx(s.title, s.h2, className)}>{children}</h2>;
    case titleSizes.H3:
      return <h3 className={cx(s.title, s.h3, className)}>{children}</h3>;
    default:
      return <h2 className={cx(s.title, s.h1, className)}>{children}</h2>;
  }
}

Title.H1 = titleSizes.H1;
Title.H2 = titleSizes.H2;
Title.H3 = titleSizes.H3;

Title.propTypes = {
  /** Element or text to render as main title */
  children: PropTypes.node.isRequired,

  /** adds class to component */
  className: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),

  /** Set Title.{H1,H2,H3} of the html title tag */
  size: PropTypes.oneOf([titleSizes.H1, titleSizes.H2, titleSizes.H3])
    .isRequired
};
