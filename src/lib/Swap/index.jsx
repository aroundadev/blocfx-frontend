import React from "react";
import cx from "classnames";
import Circle from "./circle.svg";
import s from "./style.module.scss";

export default function Swap({ className, onClick }) {
  return (
    <div className={cx(s.SwapButton, className)}>
      <div onClick={onClick}>
        <Circle className={s.Circle} />
      </div>
    </div>
  );
}
