Tabs component example:

```jsx
import { Tab, TabTitle, TabContent } from "lib/Tabs";
<Tabs>
  <Tab>
    <TabTitle>Beneficiarie 1</TabTitle>
    <TabContent>
      Wonderful, Harmony and very impressive, in the depths of Christian believe
      A Blessed weekend to all
    </TabContent>
  </Tab>
  <Tab>
    <TabTitle>Beneficiarie 2</TabTitle>
    <TabContent>
      Thank you for your cultural openness and for your great generosity in
      posting all those masterpieces of history for all of us!
    </TabContent>
  </Tab>
  <Tab>
    <TabTitle>Beneficiarie 3</TabTitle>
    <TabContent>
      Marvelous!!!!!... A magnificent composition.. The chorus are just
      thrilling.... I also read that this sort of chants a quite useful to turn
      negative into a positive energies around,.. at certain Hrz volume
    </TabContent>
  </Tab>
</Tabs>;
```
