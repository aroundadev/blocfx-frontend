import React, { useState } from "react";
import cx from "classnames";

import s from "./style.module.scss";

const findComponentByName = name => list =>
  list.props.children.find(child => {
    return child.type.name === name;
  });

const getTabTitle = findComponentByName("TabTitle");
const getTabContent = findComponentByName("TabContent");

export default function Tabs({ children, startTab, className }) {
  const [active, setActive] = useState(startTab);

  if (!Array.isArray(children)) {
    children = [children];
  }
  const titles = children.map(c => getTabTitle(c));
  const tabs = children.map(c => getTabContent(c));

  const clickHandler = tabIndex => {
    setActive(tabIndex);
  };

  return (
    <div className={cx(s.wrapper, className)}>
      <div className={s.titles}>
        {titles.map((title, i) => (
          <div
            className={cx(s.title, i === active ? s.active : undefined)}
            key={i}
            onClick={clickHandler.bind(null, i)}
          >
            {title.props.children}
          </div>
        ))}
      </div>
      {tabs[active]}
    </div>
  );
}

function Tab({ children }) {
  return <div>{children}</div>;
}

function TabTitle({ children, className }) {
  return <div className={cx(s.title, className)}>{children}</div>;
}

function TabContent({ children, className }) {
  return <div className={cx(s.content, className)}>{children}</div>;
}

export { Tab, TabTitle, TabContent };
