import React from "react";
import cx from "classnames";

import CircleArrows from "./circle-arrows.svg";

import s from "./style.module.scss";

export default function ResetButton({ className }) {
  return (
    <div className={cx(s.resetButton, className)}>
      <div>30 seconds to update rates feed </div>
      <CircleArrows />
    </div>
  );
}
