import React from "react";
import cx from "classnames";

import MenuItem from "lib/Menu/MenuItem";

import LogoutIcon from "../svg/logout.svg";

const itemsBottom = [
  {
    name: "Logout",
    link: "/login",
    icon: LogoutIcon
  }
];

export default function Logout({ className }) {
  return (
    <ul className={className}>
      {itemsBottom.map((item, i) => (
        <MenuItem key={i}>{item}</MenuItem>
      ))}
    </ul>
  );
}
