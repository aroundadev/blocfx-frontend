import React from "react";
import { NavLink } from "react-router-dom";

import s from "./style.module.scss";

export default function MenuItem({ children }) {
  const { name, link, icon } = children;

  return (
    <NavLink
      isActive={(match, location) => {
        return !!match;
      }}
      activeStyle={{
        backgroundColor: "pink"
      }}
      activeClassName={s.activeLink}
      className={s.link}
      to={link}
    >
      <li className={s.item}>
        {icon({ className: s.icon })}
        <div className={s.text}>{name}</div>
      </li>
    </NavLink>
  );
}
