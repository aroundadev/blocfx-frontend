import React from "react";
import { NavLink } from "react-router-dom";
import cx from "classnames";

import DetectClickOutside from "lib/DetectClickOutside";
import CloseMenuButton from "lib/CloseMenuButton";

import ConverterIcon from "../svg/converter.svg";
import OTCDeskIcon from "../svg/otc.svg";
import PaymentAccountsIcon from "../svg/payment_accounts.svg";
import BeneficiariesIcon from "../svg/beneficiary.svg";
import HistoryIcon from "../svg/history.svg";
import SupportIcon from "../svg/support.svg";
import SettingsIcon from "../svg/settings.svg";
import LogoutIcon from "../svg/logout.svg";

import s from "./style.module.scss";

const itemsTop = [
  {
    name: "Converter",
    link: "/exchanger/converter",
    icon: ConverterIcon
  },
  {
    name: "OTC Desk",
    link: "/exchanger/otc-desk",
    icon: OTCDeskIcon
  },
  {
    name: "Payment Accounts",
    link: "/exchanger/payment-accounts",
    icon: PaymentAccountsIcon
  },
  {
    name: "Beneficiaries",
    link: "/exchanger/beneficiaries",
    icon: BeneficiariesIcon
  }
];

const itemsMiddle = [
  {
    name: "History",
    link: "/exchanger/history",
    icon: HistoryIcon
  },
  {
    name: "Support",
    link: "/exchanger/support",
    icon: SupportIcon
  },
  {
    name: "Settings",
    link: "/exchanger/settings",
    icon: SettingsIcon
  }
];

const itemsBottom = [
  {
    name: "Logout",
    link: "/login",
    icon: LogoutIcon
  }
];

export default function ToggleMenu({ closeMenu }) {
  return (
    <DetectClickOutside>
      {isOutside => {
        if (isOutside) {
          closeMenu(closeMenu);
        }
        return (
          <div className={s.wrapper}>
            <CloseMenuButton
              onClick={closeMenu}
              className={s.closeMenuButton}
            />
            <Menu closeMenu={closeMenu} />
          </div>
        );
      }}
    </DetectClickOutside>
  );
}

function Menu({ closeMenu }) {
  let key = 0;

  const handleClick = e => {
    closeMenu();
  };

  return (
    <>
      <ul className={s.itemsTop}>
        {itemsTop.map(item => (
          <MenuItem onClick={handleClick} key={key++}>
            {item}
          </MenuItem>
        ))}
      </ul>
      <ul className={s.itemsMiddle}>
        {itemsMiddle.map(item => (
          <MenuItem onClick={handleClick} key={key++}>
            {item}
          </MenuItem>
        ))}
      </ul>
      <ul className={s.itemsBottom}>
        {itemsBottom.map(item => (
          <MenuItem onClick={handleClick} key={key++}>
            {item}
          </MenuItem>
        ))}
      </ul>
    </>
  );
}

function MenuItem({ onClick, children }) {
  const { name, link, icon } = children;

  return (
    <NavLink
      isActive={(match, location) => {
        return !!match;
      }}
      activeStyle={{
        backgroundColor: "pink"
      }}
      activeClassName={s.activeLink}
      className={s.link}
      to={link}
    >
      <li onClick={onClick} className={s.item}>
        {icon({ className: s.icon })}
        <div className={s.text}>{name}</div>
      </li>
    </NavLink>
  );
}
