import React from "react";

import cx from "classnames";

import s from "./style.module.scss";

export default function CloseMenuButton({ className, onClick }) {
  return <div onClick={onClick} className={cx(className, s.wrapper)}></div>;
}
