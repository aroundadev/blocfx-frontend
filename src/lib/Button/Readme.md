Button component example:

```jsx
<div
  style={{
    backgroundColor: "gray",
    padding: 40,
    display: "grid",
    gridGap: 20
  }}
>
  <Button type="login">Log in</Button>
  <Button type="login" disabled>
    Log in
  </Button>

  <Button type="small">Add Beneficiary</Button>
  <Button type="small" secondary>
    Add Beneficiary
  </Button>
  <Button type="medium">Save Changes</Button>
  <Button type="medium" secondary>
    Save Changes
  </Button>
  <Button type="large">Save Changes</Button>
  <Button type="large" secondary>
    Save Changes
  </Button>
</div>
```
