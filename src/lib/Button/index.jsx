import React from "react";
import cx from "classnames";
import s from "./style.module.scss";

import DownloadIcon from "./download.svg";
import PlusIcon from "./plus.svg";

const types = {
  download: s.download,
  small: s.small,
  medium: s.medium,
  large: s.large,
  login: s.login
};

export default function Button({
  type = "primary",
  className,
  onClick,
  children,
  secondary,
  disabled
}) {
  return (
    <button
      className={cx(
        s.button,
        types[type],
        secondary && s.secondary,
        disabled && s.disabled,
        className
      )}
      onClick={onClick}
    >
      {type === "download" && <DownloadIcon className={s.icon} />}
      {type === "small" && <PlusIcon className={s.icon} />}

      <span className={s.text}>{children}</span>
    </button>
  );
}
