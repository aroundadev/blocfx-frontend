import React, { useState } from "react";
import cx from "classnames";

import DetectClickOutside from "lib/DetectClickOutside";

import OpenIcon from "./svg/settings-open.svg";
import CloseIcon from "./svg/settings-close.svg";
import DeleteIcon from "./svg/settings-delete.svg";
import EditIcon from "./svg/settings-edit.svg";

import s from "./style.module.scss";

const defaultOptions = [
  {
    name: "Edit",
    icon: EditIcon,
    className: s.editIcon
  },
  {
    name: "Delete",
    icon: DeleteIcon,
    className: s.deleteIcon
  }
];

const getOption = ({ name, icon, className }) => {
  return (
    <>
      {icon({ className })}
      <span className={s.name}>{name}</span>
    </>
  );
};

export default function SettingsMenu({
  options = defaultOptions,
  menuIsOpen = false,
  onChange = n => n,
  style,
  className
}) {
  const [isOpen, setIsOpen] = useState(menuIsOpen);

  const toggleIsOpen = () => setIsOpen(!isOpen);
  const closeMenu = () => setIsOpen(false);

  const handleMenuClick = (option, e) => {
    closeMenu();
    onChange(option.name);
  };

  return (
    <div style={style} className={cx(s.container, className)}>
      <div onMouseDown={toggleIsOpen} className={s.openButton}>
        {isOpen ? (
          <>
            <CloseIcon className={s.closeIcon} />
          </>
        ) : (
          <OpenIcon className={s.openIcon} />
        )}
      </div>
      {isOpen && (
        <DetectClickOutside className={s.menuContainer}>
          {clickedOutside => {
            if (clickedOutside) {
              closeMenu();
            }
            return (
              <ul className={s.menu}>
                {options.map((option, i) => (
                  <li
                    key={i}
                    className={s.menuItem}
                    onMouseDown={handleMenuClick.bind(null, option)}
                  >
                    {getOption(option)}
                  </li>
                ))}
              </ul>
            );
          }}
        </DetectClickOutside>
      )}
    </div>
  );
}
