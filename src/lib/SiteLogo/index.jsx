import React from "react";
import LogoSVG from "./logo.svg";

import cx from "classnames";

import s from "./style.module.scss";

const colors = {
  light: s.light,
  dark: s.dark
};

export default function SiteLogo({ color, className, style }) {
  return (
    <LogoSVG style={style} className={cx(s.logo, colors[color], className)} />
  );
}
