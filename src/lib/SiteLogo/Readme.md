Logo component example:

```jsx
<div
  style={{
    backgroundColor: "#aaa",
    padding: "30px",
    display: "grid",
    gridTemplateColumns: "1fr 1fr",
    gridGap: "30px"
  }}
>
  <SiteLogo color="dark" />
  <SiteLogo color="light" />
</div>
```
