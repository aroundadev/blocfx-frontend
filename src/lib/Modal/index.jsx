import React from "react";
import Portal from "containers/Portal";
import s from "./style.module.scss";
import Button from "lib/Button";

export default function Modal({ children }) {
  return (
    <Portal>
      <div className={s.overlay}>
        <div className={s.container}>{children}</div>
      </div>
    </Portal>
  );
}
