import React from "react";
import Button from "lib/Button";
import s from "./style.module.scss";

export default function ModalButtons({ primaryAction, secondaryAction }) {
  return (
    <div className={s.buttons}>
      <Button onClick={secondaryAction} type="medium" secondary>
        Back
      </Button>
      <Button onClick={primaryAction} type="medium">
        Save
      </Button>
    </div>
  );
}
