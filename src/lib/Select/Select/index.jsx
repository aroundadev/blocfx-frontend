import React, { useEffect, useState, useRef } from "react";
import PropTypes, { oneOfType } from "prop-types";
import cx from "classnames";

import DetectClickOutside from "lib/DetectClickOutside";

import s from "./style.module.scss";
import Indicator from "./indicator.svg";
import SearchIcon from "./search.svg";

const defaultOptions = ["option 1", "option 2", "option 3"];

export default function Select({
  className,
  style,
  options = defaultOptions,
  getOption = o => o,
  getSingleValue = o => o,
  name,
  value,
  onChange,
  searchFilter,
  menuIsOpen,
  menuButton,
  menuButtonAction,
  placeholder = "Select item ...",
  disabled,
  defaultValue
}) {
  const [isOpen, setIsOpen] = useState(menuIsOpen);
  const [selected, setSelected] = useState(
    (defaultValue && getSingleValue(defaultValue)) || placeholder
  );
  const [searchText, setSearcText] = useState(null);

  const handleMouseDown = () => {
    if (disabled) return;

    //toggle menu state
    setIsOpen(!isOpen);

    //clear filtering options
    setSearcText(null);
  };

  const closeMenu = () => setIsOpen(menuIsOpen);

  const handleOptionClick = option => {
    if (onChange) {
      onChange(option);
    }
    setSelected(getSingleValue(option));
  };

  const searchInputHandler = e => {
    setSearcText(e.target.value);
  };

  const findOptionByValue = value => {
    const valueStr = JSON.stringify(value);
    const found = options.filter(option => JSON.stringify(option) === valueStr);
    const option = found.length > 0 ? found[0] : "Select ...";
    return option;
  };

  const filterOptions = (searchText, searchFilter, options) => {
    if (searchText && searchFilter) {
      return options.filter(option => searchFilter(searchText, option));
    }
    return options;
  };

  useEffect(() => {
    if (value) {
      setSelected(getSingleValue(findOptionByValue(value)));
    }
  }, [value]);

  const filteredOptions = filterOptions(searchText, searchFilter, options);

  return (
    <div
      onMouseDown={handleMouseDown}
      className={cx(s.container, className)}
      style={style}
    >
      {/* SINGLE VALUE */}
      <div className={s.singleValue}>{selected}</div>

      {!disabled && (
        <div className={s.indicatorContainer}>
          <Indicator className={cx(s.indicator, isOpen && s.up)} />
        </div>
      )}
      {isOpen && (
        <DetectClickOutside>
          {clickedOutside => {
            if (clickedOutside) {
              closeMenu();
            }
            return (
              <ul className={s.menu} style={{}}>
                {menuButton && (
                  <li
                    key={0}
                    className={cx(s.menuItem, s.menuButton)}
                    onMouseDown={menuButtonAction}
                  >
                    {menuButton}
                  </li>
                )}

                {searchFilter && (
                  <li key={1} className={cx(s.menuItem, s.searchMenuItem)}>
                    <input
                      className={s.search}
                      onMouseDown={e => e.stopPropagation()}
                      onChange={searchInputHandler}
                      placeholder={"Search"}
                    />
                    <SearchIcon className={s.searchIcon} />
                  </li>
                )}
                {filteredOptions.map((option, i) => (
                  <li
                    className={s.menuItem}
                    key={i + 2}
                    onMouseDown={handleOptionClick.bind(null, option)}
                  >
                    {getOption(option)}
                  </li>
                ))}
              </ul>
            );
          }}
        </DetectClickOutside>
      )}
    </div>
  );
}

Select.propTypes = {
  /** Array of select options */
  options: PropTypes.array,

  /** Pass here function that returns component for the option */
  getOption: PropTypes.func,

  /** Pass here function that returns component for the selected value */
  getSingleValue: PropTypes.func,

  /** class name of the component container */
  className: PropTypes.string,

  /** Search filter function */
  searchFilter: PropTypes.func
};

export { defaultOptions };
