SelectCurrency example:

with default props:

```jsx
<div
  style={{
    height: 130
  }}
>
  <Select />
  <p>Some other content...</p>
</div>
```

more complex usage example:

```jsx
import { useState } from "react";
const options = [
  {
    name: "US Dollar",
    code: "USD",
    sign: "$"
  },
  {
    name: "Pound Sterling",
    code: "GBP",
    sign: "£"
  },
  {
    name: "Euro",
    code: "EUR",
    sign: "€"
  },
  {
    name: "Bitcoin",
    code: "BTC"
  },
  {
    name: "Etherium",
    code: "ETH"
  }
];

const getOption = ({ code, name }) => (
  <div>
    <span>{code}</span> <span>{name}</span>
  </div>
);

const Swap = () => {
  const [vals, setVals] = useState({ src: options[0], dest: options[1] });
  return (
    <>
      <Select
        value={vals.src}
        options={options}
        getOption={getOption}
        getSingleValue={getOption}
        onChange={option => setVals({ ...vals, src: option })}
      />

      <button
        onClick={() => setVals({ src: vals.dest, dest: vals.src })}
        style={{ padding: 10 }}
      >
        Swap
      </button>

      <Select
        value={vals.dest}
        options={options}
        getOption={getOption}
        getSingleValue={getOption}
        onChange={option => setVals({ ...vals, dest: option })}
      />
    </>
  );
};

<div
  style={{
    height: 200,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "flex-start"
  }}
>
  <Swap />
</div>;
```

Searchable select

```jsx
const options = [
  {
    name: "US Dollar",
    code: "USD",
    sign: "$"
  },
  {
    name: "Pound Sterling",
    code: "GBP",
    sign: "£"
  },
  {
    name: "Euro",
    code: "EUR",
    sign: "€"
  },
  {
    name: "Bitcoin",
    code: "BTC"
  },
  {
    name: "Etherium",
    code: "ETH"
  }
];

const getOption = ({ code, name }) => (
  <div>
    <span>{code}</span> <span>{name}</span>
  </div>
);

const searchFilter = (searchText, option) => {
  return option.name.toLowerCase().includes(searchText.toLowerCase());
};

<div
  style={{
    display: "grid",
    gridTemplateColumns: "1fr 125px 73px",
    gridTemplateRows: "auto auto",
    gridGap: "10px"
  }}
>
  <Select
    style={{
      gridColumn: "1 / -1"
    }}
    searchFilter={searchFilter}
    options={options}
    getOption={getOption}
    getSingleValue={getOption}
  />

  <Select
    style={{
      gridColumn: "2 / 3",
      gridRow: "2"
    }}
    searchFilter={searchFilter}
    options={options}
    getOption={getOption}
    getSingleValue={getOption}
  />

  <Select
    style={{
      gridColumn: "3 / 4",
      gridRow: "2"
    }}
    searchFilter={searchFilter}
    options={options}
    getOption={getOption}
    getSingleValue={getOption}
  />
</div>;
```
