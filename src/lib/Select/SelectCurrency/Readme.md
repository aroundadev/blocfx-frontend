SelectCurrency example:

```jsx
<div
  style={{
    display: "grid",
    gridTemplateColumns: "1fr  143px",
    gridColumnGap: "10px"
  }}
>
  <SelectCurrency />
  <SelectCurrency />
</div>
```
