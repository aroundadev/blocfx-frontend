export default [
  {
    name: "US Dollar",
    code: "USD",
    sign: "$"
  },
  {
    name: "Pound Sterling",
    code: "GBP",
    sign: "£"
  },
  {
    name: "Euro",
    code: "EUR",
    sign: "€"
  },
  {
    name: "Bitcoin",
    code: "BTC",
    icon: "bitcoin"
  },
  {
    name: "Etherium",
    code: "ETH",
    icon: "etherium"
  }
];
