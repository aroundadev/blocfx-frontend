import React, { useState } from "react";
import cx from "classnames";

import Select from "lib/Select/Select";

import defaultOptions from "./default-options";

import s from "./style.module.scss";

import BitcoinIcon from "./svg/bitcoin.svg";
import EtheriumIcon from "./svg/etherium.svg";

const defaultIcons = {
  bitcoin: BitcoinIcon,
  etherium: EtheriumIcon
};

const getOption = ({ code, name, sign, icon }) => (
  <>
    {(sign && <span className={s.sign}>{sign}</span>) ||
      (icon && defaultIcons[icon]({ className: s.icon }))}
    <span className={s.code}>{code}</span>
    <span className={s.name}>{name}</span>
  </>
);

const getSingleValue = ({ code, sign, icon }) => (
  <>
    {(sign && <span className={s.signSingleValue}>{sign}</span>) ||
      (icon && defaultIcons[icon]({ className: s.iconSingleValue }))}
    <span className={s.codeSingleValue}>{code}</span>
  </>
);

export default function SelectCurrency({
  options = defaultOptions,
  name,
  value,
  onChange,
  className,
  menuIsOpen,
  disabled,
  showLabel = true,
  dynamicLabel = true,
  label,
  placeholder
}) {
  const [labelText, setLabelText] = useState(
    (label && showLabel && label) || null
  );

  const changeHandler = option => {
    if (dynamicLabel) {
      setLabelText(option.name);
    }
    if (onChange && typeof onChange === "function") {
      onChange({ target: { name: name, value: option } });
    }
  };

  return (
    <div className={cx(s.container, className)}>
      {showLabel && (
        <label
          className={cx(
            s.label,
            (dynamicLabel && s.dynamicLabel) || s.staticLabel
          )}
        >
          {labelText}
        </label>
      )}

      <Select
        options={options}
        getOption={getOption}
        getSingleValue={getSingleValue}
        onChange={changeHandler}
        menuIsOpen={menuIsOpen}
        name={name}
        value={value}
        disabled={disabled}
        placeholder={placeholder}
      />
    </div>
  );
}
