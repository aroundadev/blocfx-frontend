export default [
  {
    name: "United States of America",
    code: "USA"
  },
  {
    name: "Great Brittain",
    code: "GB"
  },
  {
    name: "European Union",
    code: "EU"
  },
  {
    name: "Japan",
    code: "JP"
  }
];
