Select country example

```jsx
<div
  style={{
    display: "grid",
    gridTemplateColumns: "125px 73px",
    justifyContent: "end",
    gridColumnGap: "10px"
  }}
>
  <SelectCountry menuIsOpen />
  <SelectCountry />
</div>
```
