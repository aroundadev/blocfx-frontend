import React from "react";

import Select from "lib/Select/Select";

import GB from "./svg/gb.svg";
import US from "./svg/us.svg";
import EU from "./svg/eu.svg";
import JP from "./svg/jp.svg";

import defaultOptions from "./default-options";

import s from "./style.module.scss";

const defaultIcons = {
  GB: GB,
  USA: US,
  EU: EU,
  JP: JP
};

const searchFilter = (searchText, option) => {
  return option.name.toLowerCase().includes(searchText.toLowerCase());
};

const getOption = ({ name, code }) => {
  return (
    <>
      {defaultIcons[code]({ className: s.icon })}
      <span className={s.code}>{code}</span>
      <span className={s.name}>{name}</span>
    </>
  );
};

const getSingleValue = ({ name, code }) => {
  return (
    <>
      {defaultIcons[code]({ className: s.iconSingleValue })}
      <span className={s.code}>{code}</span>
    </>
  );
};

export default function SelectCountry({ menuIsOpen, className }) {
  return (
    <Select
      className={className}
      searchFilter={searchFilter}
      options={defaultOptions}
      defaultValue={defaultOptions[0]}
      getOption={getOption}
      getSingleValue={getSingleValue}
      menuIsOpen={menuIsOpen}
    />
  );
}
