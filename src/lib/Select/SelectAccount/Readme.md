Select account example:

```jsx
const accs = [
  {
    name: "Reiffeisen Bank",
    code: "USD"
  },
  {
    name: "Unicredit Bank",
    code: "USD"
  }
];
const benefic = [
  {
    name: "John Doe",
    code: "ETH"
  },
  {
    name: "Jane Sanders",
    code: "BTC"
  }
];
<div style={{ display: "flex", justifyContent: "space-between" }}>
  <SelectAccount
    options={accs}
    menuButton="+ Add payment account"
    menuButtonAction={() => console.log("adds payment account")}
    placeholder="Select payment account"
  />

  <SelectAccount
    options={benefic}
    menuButton="+ Add beneficiary"
    menuButtonAction={() => console.log("adds beneficiary")}
    placeholder="Select beneficiary"
  />
</div>;
```
