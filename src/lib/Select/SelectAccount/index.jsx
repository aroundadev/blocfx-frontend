import React from "react";
import Select from "lib/Select/Select";

import s from "./style.module.scss";

const defaultOptions = [
  {
    name: "Reiffeisen Bank",
    code: "USD"
  },
  {
    name: "Unicredit Bank",
    code: "USD"
  },
  {
    name: "John Doe",
    code: "ETH"
  },
  {
    name: "Jane Sanders",
    code: "BTC"
  }
];

const getOptions = ({ name, code }) => {
  return (
    <>
      <span className={s.left}>{name}</span>
      <span className={s.right}>{code}</span>
    </>
  );
};

const getSingleValue = ({ name }) => {
  return <span className={s.singleValue}>{name}</span>;
};

export default function SelectAccount({
  options = defaultOptions,
  menuButton,
  menuButtonAction,
  placeholder = "Select account ...",
  className,
  style
}) {
  return (
    <Select
      style={style}
      className={className}
      getOption={getOptions}
      getSingleValue={getSingleValue}
      options={options}
      menuButton={menuButton}
      menuButtonAction={menuButtonAction}
      placeholder={placeholder}
    />
  );
}
