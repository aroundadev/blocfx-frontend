Card component example

```jsx
const Visible = ({ className }) => (
  <div className={className}>
    {[1, 2, 3, 4].map((item, i) => (
      <div key={i}>{item}</div>
    ))}
  </div>
);

<Card
  style={{
    margin: 40
  }}
>
  <Visible />
</Card>;
```
