import React from "react";
import cx from "classnames";

import s from "./style.module.scss";

export default function Card({ style, className, children }) {
  return (
    <div style={style} className={cx(s.card, className)}>
      {children}
    </div>
  );
}
