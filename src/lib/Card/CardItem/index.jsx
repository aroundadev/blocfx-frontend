import React from "react";
import { Context } from "react-responsive";
import cx from "classnames";

import SettingsMenu from "lib/SettingsMenu";

import s from "./style.module.scss";
console.log(s);

export default function CardItem({ className, label, value, qrcodeUrl }) {
  return (
    <>
      <div className={cx(s.cardItem, className)}>
        <p className={s.label}>{label}</p>
        <p className={cx(s.value, s.withQrCode)}>
          {qrcodeUrl && (
            <span>
              <img className={s.qrcode} src={qrcodeUrl} />
            </span>
          )}
          <span>{value}</span>
        </p>
      </div>
    </>
  );
}
