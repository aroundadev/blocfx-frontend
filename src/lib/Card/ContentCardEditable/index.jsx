import React, { useState } from "react";
import cx from "classnames";

import SettingsMenu from "lib/SettingsMenu";

import IndicatorIcon from "./indicator.svg";

import s from "./style.module.scss";

export default function ContentCardEditable({
  visible,
  hidden,
  showDetails = true,
  style,
  className,
  onMenuChange
}) {
  const [isOpen, setIsOpen] = useState(false);

  const handleClick = () => setIsOpen(!isOpen);

  return (
    <div style={style} className={cx(s.card, className)}>
      {visible({ className: s.visible })}
      {isOpen && hidden && hidden({ className: s.hidden })}
      {showDetails && (
        <DropdownButton handleClick={handleClick} isOpen={isOpen} />
      )}

      <SettingsMenu
        style={
          showDetails
            ? {
                gridRow: 1,
                gridColumn: 2
              }
            : {
                gridRow: 1,
                gridColumn: 3
              }
        }
        className={s.settingsMenu}
        onChange={onMenuChange}
      />
    </div>
  );
}

function DropdownButton({ handleClick, isOpen, className }) {
  return (
    <div
      onMouseDown={handleClick}
      className={cx(s.dropdownButton, isOpen && s.open)}
    >
      <div className={s.details}>Details</div>
      <IndicatorIcon className={cx(s.indicatorIcon, isOpen && s.open)} />
    </div>
  );
}
