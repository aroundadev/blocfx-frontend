import React, { useState } from "react";
import cx from "classnames";
import s from "./style.module.scss";

import IndicatorIcon from "./indicator.svg";

const DropdownButton = ({ onClick, isOpen }) => {
  return (
    <div className={cx(s.dropdownButton, isOpen && s.dash)} onClick={onClick}>
      <IndicatorIcon className={cx(s.indicatorIcon, isOpen && s.up)} />
    </div>
  );
};

export default function Hidden({ children }) {
  const [isOpen, setIsOpen] = useState(false);

  const handleClick = () => setIsOpen(!isOpen);

  return (
    <>
      <DropdownButton onClick={handleClick} isOpen={isOpen} />
      <div className={cx(s.hidden, isOpen && s.show)}>{children}</div>
    </>
  );
}
