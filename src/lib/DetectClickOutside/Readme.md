Click outside the gray square, then click inside of it:

```js
const style = {
  padding: "1em",
  background: "#eee",
  border: "1px solid gray"
};

<DetectClickOutside>
  {isOutside => {
    return <div style={style}>{(isOutside && "OUTSIDE") || "INSIDE"}</div>;
  }}
</DetectClickOutside>;
```

```js
const style = {
  padding: "1em",
  background: "#eee",
  border: "1px solid gray"
};

<DetectClickOutside>
  <div style={style}>Click outside to hide me</div>
</DetectClickOutside>;
```
