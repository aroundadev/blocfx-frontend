Input component example

```jsx
<div
  style={{
    display: "grid",
    gap: "40px"
  }}
>
  <Input label={"Email"} placeholder={"Enter email"} />
  <Input
    error="Please enter the right value"
    label={"Email"}
    placeholder={"Enter email"}
  />
</div>
```
