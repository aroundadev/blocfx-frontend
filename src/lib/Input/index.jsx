import React from "react";
import cx from "classnames";
import s from "./style.module.scss";

import ErrTriangle from "./ErrTriangle.svg";

export default function Input({
  className,
  name,
  label,
  placeholder,
  onChange,
  disabled,
  value,
  error = false,
  type = "text"
}) {
  return (
    <div className={cx(s.container, className)}>
      <div className={s.info}>
        <div className={s.label}>{label}</div>
        {error && (
          <div className={s.error}>
            <ErrTriangle className={s.errorIcon} />
            <span className={s.errorMessage}>{error}</span>
          </div>
        )}
      </div>
      <input
        value={value}
        name={name}
        onChange={onChange}
        className={s.input}
        placeholder={placeholder}
        disabled={disabled}
        type={type}
      />
    </div>
  );
}
