import React from "react";
import { useHistory } from "react-router-dom";
import cx from "classnames";

import Title from "lib/Title";
import Card from "lib/Card/Card";
import InputDisabled from "lib/InputDisabled";
import Panel from "containers/Panel";
import Button from "lib/Button";
import Hidden from "lib/Card/Hidden";
import CardItem from "lib/Card/CardItem";
import TermsPane from "../../components/TermsPane";

import s from "./style.module.scss";

const dollar = {
  name: "US Dollar",
  code: "USD",
  sign: "$"
};

const euro = {
  name: "Euro",
  code: "EUR",
  sign: "€"
};

export default function FiatToken() {
  const history = useHistory();
  const handleClick = () => history.push("/exchanger/fiat-token/payment");
  return (
    <>
      <Title className={s.title} size={"h1"}>
        Agreement
      </Title>
      <Card className={cx(s.section, s.exchange)}>
        <Panel className={s.panel}>
          <InputDisabled
            className={s.value}
            label={"You send"}
            value={"200.00"}
          />
          <InputDisabled className={s.currency} value={dollar} />
          <p className={s.info}>Transaction ID 122325898</p>
        </Panel>
        <Panel className={s.panel}>
          <InputDisabled
            className={s.value}
            label={"You get"}
            value={"181.05"}
          />
          <InputDisabled className={s.currency} value={euro} />
          <p className={s.info}>1 ETH = 198.39 USD</p>
        </Panel>
      </Card>

      {/* PAYMENT ACCOUNT */}
      <Title className={s.subtitle} size={"h2"}>
        Payment Account
      </Title>
      <Card className={s.section}>
        <div className={s.cardItemsContainer}>
          <CardItem
            className={s.cardItem}
            label="Bank name"
            value="Raiffeisen Bank"
          />
          <CardItem
            className={s.cardItem}
            label="Bank account number"
            value="5168 2323 4123 4219"
          />
          <CardItem className={s.cardItem} label="Currency" value="EUR" />
        </div>
        <Hidden>
          <div className={s.cardItemsContainer}>
            <CardItem
              className={s.cardItem}
              label="Sort Code"
              value="12-34-56"
            />
            <CardItem
              className={s.cardItem}
              label="Bank Full Address"
              value="Charlotte str. 48 WIT"
            />
            <CardItem
              className={s.cardItem}
              label="Location"
              value="Vienna, Austria"
            />
          </div>
        </Hidden>
      </Card>

      {/* BENEFICIARY */}

      <Title className={s.subtitle} size={"h2"}>
        Beneficiary
      </Title>
      <Card className={s.section}>
        <div className={s.cardItemsContainer}>
          <CardItem className={s.cardItem} label="Name" value="John Doe" />
          <CardItem
            className={s.cardItem}
            label="Bank name"
            value="Raiffeisen Bank"
          />
          <CardItem
            className={s.cardItem}
            label="Account number"
            value="4112 4274 9123 5676"
          />
          <CardItem className={s.cardItem} label="Currency" value="EUR" />
        </div>
        <Hidden>
          <div className={s.cardItemsContainer}>
            <CardItem
              className={s.cardItem}
              label="Sort Code"
              value="12-34-56"
            />
            <CardItem
              className={s.cardItem}
              label="Bank Full Address"
              value="Charlotte str. 48 WIT"
            />
          </div>
        </Hidden>
      </Card>

      <TermsPane />

      <div className={s.buttons}>
        <div className={s.info}>
          By clicking Continue you agree with{" "}
          <a className={s.link} href="#terms">
            Terms of Use and Privacy Policy
          </a>
        </div>
        <Button onClick={handleClick} className={s.acceptButton} type="primary">
          Accept Rates
        </Button>
      </div>
    </>
  );
}

function PaymentAccountVisible() {
  return "Visible";
}

function PaymentAccountHidden() {
  return "Hidden";
}
