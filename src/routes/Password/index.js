import React from "react";

import Input from "lib/Input";
import Button from "lib/Button";

import s from "./style.module.scss";

const Password = ({ handleSubmit, handleInput }) => {
    return (
        <div className={s.password}>
            <form className={s.form} onSubmit={(e) => e.preventDefault()}>
                <Input
                    name="password"
                    onChange={handleInput}
                    placeholder={"Enter your password"}
                    type="password"
                />
                <div className={s.buttonWrapper}>
                    <Button onClick={handleSubmit} type="login"> Submit </Button>
                </div>
            </form>
        </div>
    );
};

export default Password;
