import React from "react";
import cx from "classnames";
import { useHistory } from "react-router-dom";

import Title from "lib/Title";
import Button from "lib/Button";
import Tabs from "lib/Tabs";
import { Tab, TabTitle, TabContent } from "lib/Tabs";
import ContentCardEditable from "lib/Card/ContentCardEditable";
import CardItem from "lib/Card/CardItem";

import s from "./style.module.scss";

const accounts = [
  {
    type: "fiat",
    name: { label: "Name", value: "John Doe" },
    bankName: { label: "Bank Name", value: "Monobank" },
    accountNumber: { label: "Account Number", value: "5168 2233 4123 4219" },
    currency: { label: "Currency", value: "EUR" },
    sortCode: { label: "Sort Code", value: "12-34-56" },
    bankAddress: {
      label: "Bank full address",
      value: "John Charlotte str.48 WIT"
    }
  },
  {
    type: "fiat",
    name: { label: "Name", value: "John Doe" },
    bankName: { label: "Bank Name", value: "Raiffeisen Bank" },
    accountNumber: { label: "Account Number", value: "4142 4275 9123 1925" },
    currency: { label: "Currency", value: "EUR" },
    sortCode: { label: "Sort Code", value: "12-34-56" },
    bankAddress: {
      label: "Bank full address",
      value: "John Charlotte str.48 WIT"
    }
  },
  {
    type: "fiat",
    name: { label: "Name", value: "John Doe" },
    bankName: { label: "Bank Name", value: "Monobank" },
    accountNumber: { label: "Account Number", value: "5168 2233 4123 4219" },
    currency: { label: "Currency", value: "EUR" },
    sortCode: { label: "Sort Code", value: "12-34-56" },
    bankAddress: {
      label: "Bank full address",
      value: "John Charlotte str.48 WIT"
    }
  },
  {
    type: "fiat",
    name: { label: "Name", value: "John Doe" },
    bankName: { label: "Bank Name", value: "Monobank" },
    accountNumber: { label: "Account Number", value: "5168 2233 4123 4219" },
    currency: { label: "Currency", value: "EUR" },
    sortCode: { label: "Sort Code", value: "12-34-56" },
    bankAddress: {
      label: "Bank full address",
      value: "John Charlotte str.48 WIT"
    }
  },
  {
    type: "crypto",
    name: { label: "Name", value: "John Doe" },
    accountNumber: {
      label: "Account Number",
      value: "1Dq24f234bdfg544h5676aff6..."
    },
    currency: { label: "Currency", value: "ETH" }
  },
  {
    type: "crypto",
    name: { label: "Name", value: "John Doe" },
    accountNumber: {
      label: "Account Number",
      value: "876f98eabb87234abc923488b..."
    },
    currency: { label: "Currency", value: "BTC" }
  }
];

export default function Beneficiaries() {
  const history = useHistory();
  const navigate = path => history.push(path);
  const handleMenuChange = option => {
    if (option.toLowerCase() === "edit") {
      navigate("/exchanger/beneficiaries/add");
    }
  };
  return (
    <>
      <div className={s.header}>
        <Title size="h1">Beneficiaries</Title>
        <Button
          onClick={navigate.bind(null, "/exchanger/beneficiaries/add")}
          type="small"
        >
          Add Beneficiary
        </Button>
      </div>
      <Tabs className={s.tabs} startTab={0}>
        <Tab>
          <TabTitle>All</TabTitle>
          <TabContent>
            {getTabContent(accounts, null, handleMenuChange)}
          </TabContent>
        </Tab>
        <Tab>
          <TabTitle>Blockchain</TabTitle>
          <TabContent>
            {getTabContent(accounts, "crypto", handleMenuChange)}
          </TabContent>
        </Tab>
        <Tab>
          <TabTitle>Bank Account</TabTitle>
          <TabContent>
            {getTabContent(accounts, "fiat", handleMenuChange)}
          </TabContent>
        </Tab>
      </Tabs>
    </>
  );
}

function getTabContent(accs, filter, handleMenuChange) {
  let filteredAccs = accs;

  if (filter) {
    filteredAccs = accs.filter(acc => acc.type === filter);
  }

  return filteredAccs.map(acc => {
    let Visible, Hidden;
    switch (acc.type) {
      case "fiat":
        Visible = ({ className }) => (
          <div className={cx(className, s.visible)}>
            <CardItem
              className={s.cardItem}
              label={acc.name.label}
              value={acc.name.value}
            />
            <CardItem
              className={s.cardItem}
              label={acc.bankName.label}
              value={acc.bankName.value}
            />
            <CardItem
              className={s.cardItem}
              label={acc.accountNumber.label}
              value={acc.accountNumber.value}
            />
            <CardItem
              className={s.cardItem}
              label={acc.currency.label}
              value={acc.currency.value}
            />
          </div>
        );

        Hidden = ({ className }) => (
          <div className={cx(className, s.hidden)}>
            <CardItem
              className={s.cardItem}
              label={acc.sortCode.label}
              value={acc.sortCode.value}
            />
            <CardItem
              className={s.cardItem}
              label={acc.bankAddress.label}
              value={acc.bankAddress.value}
            />
          </div>
        );

        return (
          <ContentCardEditable
            className={s.card}
            visible={Visible}
            hidden={Hidden}
            onMenuChange={handleMenuChange}
          />
        );

      case "crypto":
        Visible = ({ className }) => (
          <div className={cx(className, s.visible)}>
            <CardItem
              className={s.cardItem}
              label={acc.name.label}
              value={acc.name.value}
            />
            <CardItem
              className={s.cardItem}
              label={acc.accountNumber.label}
              value={acc.accountNumber.value}
            />
            <CardItem
              className={cx(s.cardItem, s.currencyCardItem)}
              label={acc.currency.label}
              value={acc.currency.value}
            />
          </div>
        );

        return (
          <ContentCardEditable
            className={s.card}
            visible={Visible}
            editable
            showDetails={false}
            onMenuChange={handleMenuChange}
          />
        );
    }
  });
}
