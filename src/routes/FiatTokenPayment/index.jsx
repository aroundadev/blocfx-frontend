import React from "react";
import cx from "classnames";

import Title from "lib/Title";
import Card from "lib/Card/Card";
import Button from "lib/Button";
import Hidden from "lib/Card/Hidden";
import CardItem from "lib/Card/CardItem";

import s from "./style.module.scss";

const dollar = {
  name: "US Dollar",
  code: "USD",
  sign: "$"
};

const euro = {
  name: "Euro",
  code: "EUR",
  sign: "€"
};

export default function FiatTokenPayment() {
  return (
    <>
      <div className={s.titleContainer}>
        <Title className={s.title} size={"h1"}>
          Transaction done
        </Title>
        <Button type="download">Download PDF</Button>
      </div>
      <div className={s.message}>
        Convert successfully executed. Please, check your funds in the wallet.
      </div>

      {/* PAYMENT ACCOUNT */}
      <Card className={s.section}>
        <div className={s.cardItemsContainer}>
          <CardItem
            className={s.cardItem}
            label="Transaction ID"
            value="123756482"
          />
          <CardItem
            className={s.cardItem}
            label="Status"
            value="Transaction done"
          />
          <CardItem className={s.cardItem} label="You send" value="200 USD" />
          <CardItem className={s.cardItem} label="You get" value="181.05 EUR" />
        </div>
      </Card>

      {/* BENEFICIARY */}

      <Card className={s.section}>
        <div className={s.cardItemsContainer}>
          <CardItem className={s.cardItem} label="Name" value="John Doe" />
          <CardItem
            className={s.cardItem}
            label="Bank Name"
            value="Raiffeisen Bank "
          />
          <CardItem className={s.cardItem} label="Currency" value="EUR" />
          <CardItem
            className={s.cardItem}
            label="Bank account number"
            value="4112 4275 9123 1925"
          />
        </div>

        <Hidden>
          <div className={s.cardItemsContainer}>
            <CardItem
              className={s.cardItem}
              label="Sort Code"
              value="12-34-56"
            />
            <CardItem
              className={s.cardItem}
              label="Bank Full Address"
              value="Charlotte str. 48 WIT"
            />
          </div>
        </Hidden>
      </Card>
    </>
  );
}

function PaymentAccountVisible() {
  return "Visible";
}

function PaymentAccountHidden() {
  return "Hidden";
}
