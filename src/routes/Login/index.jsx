import React, { useState, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import { string, object } from "yup";

import SiteLogo from "lib/SiteLogo";
import Card from "lib/Card/Card";
import Title from "lib/Title";
import Input from "lib/Input";
import Button from "lib/Button";

import s from "./style.module.scss";

const init = {
  email: "",
  password: ""
};

const schema = object().shape({
  email: string()
    .required()
    .email(),
  password: string()
    .required()
    .min(8)
});

export default function Login() {
  const history = useHistory();
  const [formState, setFormState] = useState(init);
  const [errors, setErrors] = useState(init);

  const handleClick = e => {
    console.log(formState);

    schema.isValid(formState).then(e => {
      if (e) {
        return history.push("/exchanger/converter");
      }

      schema
        .validate(formState, { abortEarly: false })
        .then(v => setErrors(init))
        .catch(e => {
          console.log(e);

          const currentErrors = e.inner.reduce(
            (acc, val) => ({ ...acc, [val.path]: val.errors.join(", ") }),
            {}
          );
          setErrors(currentErrors);
        });
    });
  };

  const handleChange = e => {
    const { name, value } = e.target;
    setFormState({ ...formState, [name]: value });
    setErrors({ ...errors, [name]: "" });
  };

  return (
    <div className={s.wrapper}>
      <SiteLogo color="light" className={s.logo} />
      <Card className={s.card}>
        <Title className={s.title} size="h1">
          Welcome!
        </Title>
        <p className={s.text}>
          We make it easy for everyone to exchange cryptocurrency.
        </p>
        <form onSubmit={e => e.preventDefault()} className={s.form}>
          <div className={s.inputs}>
            <Input
              name="email"
              value={formState.email}
              onChange={handleChange}
              className={s.email}
              label={"Email"}
              placeholder={"Enter email"}
              error={errors.email}
            />
            <Input
              name="password"
              value={formState.password}
              onChange={handleChange}
              className={s.password}
              label={"Password"}
              placeholder={"Enter password"}
              error={errors.password}
            />
          </div>
          <Button onClick={handleClick} className={s.button} type="login">
            Log in
          </Button>
          <p className={s.register}>
            Don’t have an account? <Link to="/registration">Register</Link>
          </p>
        </form>
      </Card>
    </div>
  );
}
