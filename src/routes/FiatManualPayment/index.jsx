import React from "react";
import cx from "classnames";

import Title from "lib/Title";
import Card from "lib/Card/Card";
import Button from "lib/Button";
import Hidden from "lib/Card/Hidden";
import CardItem from "lib/Card/CardItem";
import TermsPane from "../../components/TermsPane";

import s from "./style.module.scss";

const dollar = {
  name: "US Dollar",
  code: "USD",
  sign: "$"
};
const etherium = {
  name: "Etherium",
  code: "ETH",
  icon: "etherium"
};

export default function FiatManualPayment() {
  return (
    <>
      <Title className={s.title} size={"h1"}>
        Payment
      </Title>

      {/* PAYMENT ACCOUNT */}
      <Card className={s.section}>
        <div className={s.cardItemsContainer}>
          <CardItem
            className={s.cardItem}
            label="Transaction ID"
            value="123756482"
          />
          <CardItem
            className={s.cardItem}
            label="Expected exchange rate"
            value="1 ETH = 198.39 USD"
          />
          <CardItem
            className={s.cardItem}
            label="You send"
            value="198.39 USD"
          />
        </div>
      </Card>

      {/* BENEFICIARY */}

      <Card className={s.section}>
        <div className={s.cardItemsContainer}>
          <Title className={s.subtitle} size={"h2"}>
            Send whole ammount in one transaction
          </Title>
          <CardItem
            className={s.cardItem}
            label="Bank Name"
            value="Raiffeisen Bank "
          />
          <CardItem
            className={s.cardItem}
            label="Bank account number"
            value="4112 4275 9123 1925"
          />
          <CardItem className={s.cardItem} label="Currency" value="EUR" />
          <CardItem
            className={s.cardItem}
            label="Time left to send funds"
            value="01:35:24"
          />
        </div>

        <Hidden>
          <div className={s.cardItemsContainer}>
            <CardItem
              className={s.cardItem}
              label="Sort Code"
              value="12-34-56"
            />
            <CardItem
              className={s.cardItem}
              label="Location"
              value="Vienna, Austria"
            />
            <CardItem
              className={s.cardItem}
              label="Bank Full Address"
              value="Charlotte str. 48 WIT"
            />
          </div>
        </Hidden>
      </Card>
    </>
  );
}

function PaymentAccountVisible() {
  return "Visible";
}

function PaymentAccountHidden() {
  return "Hidden";
}
