import React from "react";
import { useHistory } from "react-router-dom";

import cx from "classnames";

import Title from "lib/Title";
import Button from "lib/Button";
import Tabs from "lib/Tabs";
import { Tab, TabTitle, TabContent } from "lib/Tabs";
import ContentCardEditable from "lib/Card/ContentCardEditable";
import CardItem from "lib/Card/CardItem";

import s from "./style.module.scss";

const accounts = [
  {
    bankName: { label: "Bank Name", value: "Monobank" },
    accountNumber: { label: "Account Number", value: "5168 2233 4123 4219" },
    currency: { label: "Currency", value: "EUR" },
    sortCode: { label: "Sort Code", value: "12-34-56" },
    bankAddress: {
      label: "Bank full address",
      value: "John Charlotte str.48 WIT"
    },
    location: { label: "Location", value: "Vienna, Austria" }
  },
  {
    bankName: { label: "Bank Name", value: "Monobank" },
    accountNumber: { label: "Account Number", value: "5168 2233 4123 4219" },
    currency: { label: "Currency", value: "EUR" },
    sortCode: { label: "Sort Code", value: "12-34-56" },
    bankAddress: {
      label: "Bank full address",
      value: "John Charlotte str.48 WIT"
    },
    location: { label: "Location", value: "Vienna, Austria" }
  },
  {
    bankName: { label: "Bank Name", value: "Monobank" },
    accountNumber: { label: "Account Number", value: "5168 2233 4123 4219" },
    currency: { label: "Currency", value: "EUR" },
    sortCode: { label: "Sort Code", value: "12-34-56" },
    bankAddress: {
      label: "Bank full address",
      value: "John Charlotte str.48 WIT"
    },
    location: { label: "Location", value: "Vienna, Austria" }
  }
];

export default function PaymentAccounts() {
  const history = useHistory();
  const handleMenuChange = option => {
    if (option.toLowerCase() === "edit")
      history.push("/exchanger/payment-accounts/add");
  };
  return (
    <>
      <div className={s.header}>
        <Title size="h1">Payment Accounts</Title>
        <Button
          onClick={() => history.push("/exchanger/payment-accounts/add")}
          type="small"
        >
          Add Payment Account
        </Button>
      </div>
      <div>{getTabContent(accounts, handleMenuChange)}</div>
    </>
  );
}

function getTabContent(accs, handleMenuChange) {
  return accs.map(acc => {
    const Visible = ({ className }) => (
      <div className={cx(className, s.visible)}>
        <CardItem
          className={s.cardItem}
          label={acc.bankName.label}
          value={acc.bankName.value}
        />
        <CardItem
          className={s.cardItem}
          label={acc.accountNumber.label}
          value={acc.accountNumber.value}
        />
        <CardItem
          className={s.cardItem}
          label={acc.currency.label}
          value={acc.currency.value}
        />
      </div>
    );

    const Hidden = ({ className }) => (
      <div className={cx(className, s.hidden)}>
        <CardItem
          className={s.cardItem}
          label={acc.sortCode.label}
          value={acc.sortCode.value}
        />
        <CardItem
          className={s.cardItem}
          label={acc.bankAddress.label}
          value={acc.bankAddress.value}
        />
        <CardItem
          className={s.cardItem}
          label={acc.location.label}
          value={acc.location.value}
        />
      </div>
    );

    return (
      <ContentCardEditable
        className={s.card}
        visible={Visible}
        hidden={Hidden}
        onMenuChange={handleMenuChange}
      />
    );
  });
}
