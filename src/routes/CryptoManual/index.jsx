import React from "react";
import cx from "classnames";
import { useHistory } from "react-router-dom";

import Title from "lib/Title";
import Card from "lib/Card/Card";
import InputDisabled from "lib/InputDisabled";
import Panel from "containers/Panel";
import Button from "lib/Button";
import Hidden from "lib/Card/Hidden";
import CardItem from "lib/Card/CardItem";
import TermsPane from "../../components/TermsPane";

import s from "./style.module.scss";

const bitcoin = {
  name: "Bitcoin",
  code: "BTC",
  icon: "bitcoin"
};

const etherium = {
  name: "Etherium",
  code: "ETH",
  icon: "etherium"
};

export default function CryptoManual() {
  const history = useHistory();
  const handleClick = () => history.push("/exchanger/crypto-manual/payment");
  return (
    <>
      <Title className={s.title} size={"h1"}>
        Agreement
      </Title>
      <Card className={cx(s.section, s.exchange)}>
        <Panel className={s.panel}>
          <InputDisabled
            className={s.value}
            label={"You send"}
            value={"48.17"}
          />
          <InputDisabled className={s.currency} value={etherium} />
          <p className={s.info}>Transaction ID 122325898</p>
        </Panel>
        <Panel className={s.panel}>
          <InputDisabled className={s.value} label={"You get"} value={"1"} />
          <InputDisabled className={s.currency} value={bitcoin} />
          <p className={s.info}>48.17 ETH = 1 BTC</p>
        </Panel>
      </Card>

      {/* BENEFICIARY */}

      <Title className={s.subtitle} size={"h2"}>
        Beneficiary
      </Title>
      <Card className={s.section}>
        <div className={s.cardItemsContainer}>
          <CardItem className={s.cardItem} label="Name" value="John Doe" />
          <CardItem
            className={s.cardItem}
            label="Account number"
            value="1DH9qza7fn9snSC"
          />
          <CardItem className={s.cardItem} label="Currency" value="BTC" />
        </div>
      </Card>

      <TermsPane />

      <div className={s.buttons}>
        <div className={s.info}>
          By clicking Continue you agree with{" "}
          <a className={s.link} href="#terms">
            Terms of Use and Privacy Policy
          </a>
        </div>
        <Button onClick={handleClick} className={s.acceptButton} type="large">
          Accept Rates
        </Button>
      </div>
    </>
  );
}

function PaymentAccountVisible() {
  return "Visible";
}

function PaymentAccountHidden() {
  return "Hidden";
}
