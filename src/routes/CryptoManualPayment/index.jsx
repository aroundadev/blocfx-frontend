import React from "react";
import cx from "classnames";

import Title from "lib/Title";
import Card from "lib/Card/Card";
import CardItem from "lib/Card/CardItem";

import qrcode from "./qr.png";

console.log(qrcode);

import s from "./style.module.scss";

const dollar = {
  name: "US Dollar",
  code: "USD",
  sign: "$"
};
const etherium = {
  name: "Etherium",
  code: "ETH",
  icon: "etherium"
};

export default function CryptoManualPayment() {
  return (
    <>
      <Title className={s.title} size={"h1"}>
        Payment
      </Title>

      {/* PAYMENT ACCOUNT */}
      <Card className={s.section}>
        <div className={s.cardItemsContainer}>
          <CardItem
            className={s.cardItem}
            label="Transaction ID"
            value="123756482"
          />
          <CardItem
            className={s.cardItem}
            label="Expected exchange rate"
            value="48.17 ETH = 1 BTC"
          />
          <CardItem className={s.cardItem} label="You send" value="48.17 ETH" />
          <CardItem className={s.cardItem} label="You get" value="1 BTC" />
        </div>
      </Card>

      {/* BENEFICIARY */}

      <Card className={cx(s.section, s.beneficiary)}>
        <div className={s.cardItemsContainer}>
          <Title className={s.subtitle} size={"h2"}>
            Send whole ammount in one transaction
          </Title>
          <CardItem
            className={cx(s.cardItem, s.time)}
            label="Time left to send funds"
            value="01:35:24"
          />
          <CardItem
            className={cx(s.cardItem, s.address)}
            label="Bitcoin address"
            value="9430f1a103aa84d6f7423acf2df59…"
            qrcodeUrl={qrcode}
          />
        </div>
        <QRCode />
      </Card>
    </>
  );
}

function QRCode() {
  return (
    <div className={s.qrcode}>
      <img src={qrcode} />
    </div>
  );
}

function PaymentAccountVisible() {
  return "Visible";
}

function PaymentAccountHidden() {
  return "Hidden";
}
