import React from "react";
import { useHistory } from "react-router-dom";

import Tabs, { Tab, TabTitle, TabContent } from "lib/Tabs";
import BlockchainForm from "containers/BlockchainForm";
import BankAccForm from "containers/BankAccForm";
import Title from "lib/Title";
import Card from "lib/Card/Card";

import s from "./style.module.scss";

export default function AddBeneficiary() {
  const history = useHistory();
  const navigate = path => history.push("/exchanger/beneficiaries");
  return (
    <Card className={s.wrapper}>
      <Title className={s.title} size={Title.H1}>
        Add Beneficiary
      </Title>
      <Tabs startTab={1}>
        <Tab>
          <TabTitle>Blockchain</TabTitle>
          <TabContent>
            <BlockchainForm primButtonAction={navigate} />
          </TabContent>
        </Tab>
        <Tab>
          <TabTitle>Bank account</TabTitle>
          <TabContent>
            <BankAccForm primButtonAction={navigate} />
          </TabContent>
        </Tab>
      </Tabs>
    </Card>
  );
}
