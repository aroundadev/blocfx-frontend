import React from "react";
import Title from "lib/Title";
import Card from "lib/Card/Card";
import AddPaymentAccountForm from "containers/AddPaymentAccountForm";

import s from "./style.module.scss";

export default function AddPaymentAccount() {
  return (
    <>
      <Title className={s.title} size="h1">
        Add Payment Account
      </Title>
      <Card>
        <AddPaymentAccountForm />
      </Card>
    </>
  );
}
