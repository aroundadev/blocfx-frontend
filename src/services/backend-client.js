let axios = require('axios');

class BackendError extends Error {
    constructor(errorType, error, errorData) {
        super();
        this._errorType = errorType;
        this._error = error;
        this._errorData = errorData;
    }

    get errorType() {
        return this._errorType;
    }

    get error() {
        return this._error;
    }

    get errorData() {
        return this._errorData;
    }


    static _buildBackendError(error){
        let backendError = null;


        let response = null;
        let statusCode = 0;
        let data = null;
        if(error.response){
            response = error.response;
            if(response.status){
                statusCode = response.status;
            }
            if(response.data){
                data = response.data;
            }
        }


        if (statusCode === 401) {
            backendError = new BackendError(BackendError.AUTH_ERROR, error, data);
        }
        else if(statusCode === 400 && response && response.data && response.data.errors) {
            backendError = new ValidationError(error);
        }
        else if(statusCode === 404) {
            backendError = new BackendError(BackendError.NOT_FOUND, error);
        }
        else if(statusCode === 500) {
            backendError = new BackendError(BackendError.INTERNAL_SERVER_ERROR, error, data);
        }
        else if(statusCode === 571) {
            backendError = new BackendError(BackendError.EXTERNAL_API_STORAGE_ERROR, error, data);
        }
        else if(error.code && error.code === 'ECONNREFUSED'){
            backendError = new BackendError(BackendError.SERVER_NOT_CONNECTED, error, data);
        }
        else{
            backendError = new BackendError(BackendError.UNKNOWN_ERROR, error);
        }

        return backendError;
    }
}
BackendError.VALIDATION_ERROR = 'validation_error(400)';
BackendError.AUTH_ERROR = 'auth_error(401)';
BackendError.NOT_FOUND = 'NOT_FOUND(404)';
BackendError.INTERNAL_SERVER_ERROR = 'internal_server_error(500)';
BackendError.EXTERNAL_API_STORAGE_ERROR = 'EXTERNAL_API_STORAGE_ERROR(571)';
BackendError.SERVER_NOT_CONNECTED = 'not_connected';
BackendError.UNKNOWN_ERROR = 'unknown_error';

class ValidationError extends BackendError {
    constructor(error) {
        super(BackendError.VALIDATION_ERROR, error, error.response.data);
        this._validationErrors = error.response.data.errors;
    }

    get validationErrors() {
        return this._validationErrors;
    }
}


class BackendClient {
    constructor() {
        this.AUTH_HEADER = 'x-auth-token';
        this.debug = false;
        this.currentToken = null;

        this._needAuthExceptionHandler = () => {};

        this._booleanResponseBuilder = (response) => {
            return (response.status >= 200 && response.status < 300);
        };

        this._entitiesResponseBuilder = (response) => {
            const data = response.data;
            if(this.debug){
                console.log(data);
            }
            return data;
        };

        this._successAuthHandler = (response) => {
            const data = response.data;

            if(this.debug){
                console.log(data);
            }

            this.currentToken = response.data.token;

            return data;
        };

        this.http = axios.create({
            baseURL: 'http://localhost:3000/api/'
        });

        this.http.interceptors.response.use((response) => {
            if(this.debug){
                console.log("Response:");
                console.log(response);
            }

            return response;
        }, async (error) => {
            const serverError = BackendError._buildBackendError(error);

            if(error.config.url !== 'auth' && serverError.errorType === BackendError.AUTH_ERROR){
                const exceptionHandlerResponse = this._needAuthExceptionHandler(error);

                let retry = null;
                if(exceptionHandlerResponse instanceof Promise){
                    retry = await exceptionHandlerResponse;
                }
                else{
                    retry = exceptionHandlerResponse;
                }

                if(retry){
                    const config = error.config;
                    config.headers[this.AUTH_HEADER] = this.currentToken;
                    return await this.http.request(config);
                }
            }

            return Promise.reject(serverError);
        });
    }

    setNeedAuthExceptionHandler(handler) {
        this._needAuthExceptionHandler = handler;
    }

    _makeURLSearchParams(params) {
        const urlSearchParams = new URLSearchParams();

        Object.entries(params).forEach((value, name) => {
            urlSearchParams.append(name, value);
        });

        return urlSearchParams;
    }


    _makeFormDataForFiles(files) {
        const data = new FormData();

        files.forEach((value, name) => {
            data.append(name, value, value.name);
        });

        return data;
    }

    _getDefaultHeader(withoutToken){
        let res = {
            'Content-Type': 'application/json'
        };

        if(!withoutToken && this.currentToken) {
            res[this.AUTH_HEADER] = this.currentToken;
        }

        return res;
    }

    _makeBooleanRequest(method, url, data) {
        const config = {
            url,
            method,
            headers: this._getDefaultHeader()
        };

        if(data){
            config.data = JSON.stringify(data);
        }

        return this.http.request(config).then(this._booleanResponseBuilder);
    }

    _makeEntityGetRequest(url, params) {
        let postfix = '';
        if(params && params.length > 0){
            postfix = this._makeURLSearchParams(params);
        }

        const config = {
            url: url + '/' + postfix,
            method: 'get',
            headers: this._getDefaultHeader()
        };

        return this.http.request(config).then(this._entitiesResponseBuilder);
    }

    auth({login, password}) {
        const config = {
            method: 'post',
            url: 'auth',
            headers: this._getDefaultHeader(true),
            data: JSON.stringify(arguments[0])
        };

        return this.http.request(config).then(this._successAuthHandler);
    }

    registerUser({firstname, lastname, email, phone})  {

        const config = {
            method: 'post',
            url: 'users',
            headers: this._getDefaultHeader(true),
            params: JSON.stringify(arguments[0])
        };

        return this.http.request(config).then(this._successAuthHandler);
    }

    uploadImages({passport, selfie, details}) {

        const config = {
            method: 'post',
            url: 'uploadImg',
            headers: {
                ...this._getDefaultHeader(),
                'Content-Type': 'multipart/form-data'
            },
            data: this._makeFormDataForFiles({passport, selfie, details})
        };

        return this.http.request(config);
    }

    getHistory(from, to, status, offset) {
        let params = {};
        if(from) {
            params.from = from;
        }
        if(to) {
            params.to = to;
        }
        if(status && status !== 'all'){
            params.status = status;
        }
        if(offset){
            params.offset = offset;
        }

        return this._makeEntityGetRequest('history', params);
    }

    getTopCurrencies() {
        return this._makeEntityGetRequest('top-currencies');
    }

    getUser() {
        return this._makeEntityGetRequest('user/me');
    }

    requestChangeEmail({password, email}) {
        return this._makeBooleanRequest('post','user/email', arguments[0]);
    }

    changeEmail({code}) {
        return this._makeBooleanRequest('put','user/email/code/'+code);
    }

    changePassword({password, newPassword}) {
        return this._makeBooleanRequest('put','user/password', arguments[0]);
    }

    changePhone({password, phone}) {
        return this._makeBooleanRequest('put','user/phone', arguments[0]);
    }

    getCurrencyRates() {
        return this._makeEntityGetRequest('currency_rates');
    }

    getPayments() {
        return this._makeEntityGetRequest('payment_acc');
    }

    postPayment({
                    bid,
                    beneficearyName,
                    beneficearyAddress,
                    bankName,
                    bankAddress,
                    IBAN,
                    bankSwiftBICcode,
                    bankCountry,
                    currency,
                    reference
                }) {
        return this._makeBooleanRequest('post','payment_acc', arguments[0]);
    }

    updatePayment({}) {
        return this._makeBooleanRequest('put','payment_acc', arguments[0]);
    }

    deletePayment(id) {
        return this._makeBooleanRequest('delete','payment_acc/'+id);
    }

    getBeneficeary() {
        return this._makeEntityGetRequest('beneficeary');
    }

    postBeneficear({}) {
        return this._makeBooleanRequest('post','beneficeary', arguments[0]);
    }

    deleteBeneficear(id) {
        return this._makeBooleanRequest('delete','beneficeary/'+id);
    }

}

const demo = async () => {
    const backendClient = new BackendClient();

    backendClient.setNeedAuthExceptionHandler(async (error) => {
        await backendClient.auth({
            login: 'fake_user_dev_for_test_to_delete_after_test0.8421350478228711',
            password: 'fake_pass'
        });

        return true; //If returned true, backend client retry query
    });

    const me = await backendClient.getUser();
    console.log(me);

    //history = await backendClient.getHistory(null, null, 'one');

    console.log(await backendClient.getTopCurrencies());

    // await backendClient.changePassword({
    //     password: 'fake_pass',
    //     newPassword: 'new_fake_pass_new'
    // });



    return history;
};

const backendClient = new BackendClient();

backendClient.setNeedAuthExceptionHandler(async (error) => {
    await backendClient.auth({
        login: 'fake_user_dev_for_test_to_delete_after_test0.8421350478228711',
        password: 'fake_pass'
    });

    return true;
});

demo().then(res => console.log(res)).catch(err => console.error(err));
//modules.export = backendClient;