const path = require("path");
const express = require("express");
const cors = require("cors");

const PORT = 8080;
const STATIC_PATH = path.join(__dirname, "../dist");

const app = express();

app.use(express.static(STATIC_PATH));

app.use(cors());

app.get("*", (_, res) => {
  res.sendFile(path.join(STATIC_PATH, "index.html"));
});

app.listen(PORT, () => {
  console.log("Server is listening on port: " + PORT);
});
