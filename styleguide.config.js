const path = require("path");
module.exports = {
  components: "src/lib/**/*.jsx",
  getComponentPathLine(componentPath) {
    const dir = path.dirname(componentPath, ".jsx");
    const name = dir.split("/").slice(-1);
    const aliasedPath = dir
      .split("/")
      .slice(1)
      .join("/");

    return `import ${name} from '${aliasedPath}'`;
  },
  skipComponentsWithoutExample: true
};
